const submitButton = document.querySelector('#submit-button');
const data = {{ frappe.form_dict | json }};
const customerData = {{ customer_data | json }};

const billingFields = [
	'email',
	'billing-phone',
	'billing-given-name',
	'billing-surname',
	'billing-street-address',
	'billing-extended-address',
	'billing-locality',
	'billing-postal-code',
].reduce(function (fields, fieldName) {
	fields[fieldName] = {
		input: document.getElementById(fieldName),
		help: document.getElementById('help-' + fieldName),
		optional: !document.getElementById(fieldName).required
	};

	const field = fields[fieldName];
	const events = 'focus blur'.split(' ')
	events.forEach(event => {
		field.input.addEventListener(event, function () {
			clearFieldValidations(field);
			if (validateBillingFields()) {
				submitButton.removeAttribute('disabled');
			}
		});
	})

	return fields;
}, {});

function clearFieldValidations(field) {
	field.help.innerText = '';
	field.help.parentNode.classList.remove('has-error');
}


function validateBillingFields() {
	var isValid = true;

	Object.keys(billingFields).forEach(function (fieldName) {
		var fieldEmpty = false;
		var field = billingFields[fieldName];

		if (field.optional) {
			return;
		}

		fieldEmpty = field.input.value.trim() === '';

		if (fieldEmpty) {
			isValid = false;
			field.help.innerText = __("Field cannot be blank.");
			field.help.parentNode.classList.add('has-error');
		} else {
			clearFieldValidations(field);
		}
	});

	return isValid;
}


function prepopulate_data() {
	Object.keys(customerData).map(value => {
		if (customerData[value]) {
			billingFields[value].input.value = customerData[value];
		}
	})
}

braintree.dropin.create({
	authorization: "{{ client_token }}",
	selector: '#bt-dropin',
	threeDSecure: true,
	paypal: {
		flow: 'vault'
	},
	locale: "{{ locale }}",
	card: {
		cardholderName: {
			required: true
		},
		postalCode: {
			required: true
		},
	}
}).then(function (instance) {
	prepopulate_data()
	initiate_payment_session().then(r => {
		addListeners(instance)
	})
}).catch(function (error) {
	processError(error)
});

const addListeners = (instance) => {
	submitButton.addEventListener('click', function (e) {
		submitButton.setAttribute('disabled', 'disabled');
		submitButton.value = __('Processing...');
		e.preventDefault();
		const threeDSecureParameters = {
			amount: "{{ amount }}",
			email: billingFields.email.input.value,
			billingAddress: {
				givenName: billingFields['billing-given-name'].input.value,
				surname: billingFields['billing-surname'].input.value,
				phoneNumber: billingFields['billing-phone'].input.value.replace(/[\(\)\s\-]/g, ''), // remove (), spaces, and - from phone number
				streetAddress: billingFields['billing-street-address'].input.value,
				extendedAddress: billingFields['billing-extended-address'].input.value,
				locality: billingFields['billing-locality'].input.value,
				postalCode: billingFields['billing-postal-code'].input.value,
			}
		}

		instance.requestPaymentMethod({
			threeDSecure: threeDSecureParameters
		}).then(function (payload) {
			if (payload.liabilityShifted || payload.type == "PayPalAccount") {
				submitPayment(payload);
			} else if (payload.liabilityShiftPossible) {
				submitPayment(payload);
			} else {
				$('#warning-msg').html(__("The bank could not authenticate this card. Please try again or contact your bank."));
				$('.warning').show();
				instance.clearSelectedPaymentMethod();
			}
		}).catch(function (error) {
			processError(error)
		});
	});

	instance.on('paymentMethodRequestable', function (event) {
		if (validateBillingFields()) {
			submitButton.removeAttribute('disabled');
		}
	});

	instance.on('noPaymentMethodRequestable', function () {
		submitButton.setAttribute('disabled', 'disabled');
	});
}

const submitPayment = payload => {
	frappe.call({
		method: "payments.templates.pages.braintree_checkout.make_payment",
		type: "POST",
		freeze: true,
		headers: { "X-Requested-With": "XMLHttpRequest" },
		args: {
			"payload_nonce": payload.nonce,
			"data": data,
		}
	})
		.then(r => {
			if (r.message && r.message.status == "Completed") {
				window.location.href = r.message.redirect_to
			} else if (r.message && r.message.status == "Error") {
				window.location.href = r.message.redirect_to
			}
		})
}

const processError = error => {
	submitButton.style.display = "none"
	$('#error-msg').html(error.message);
	$('.error').show()
}

const initiate_payment_session = () => {
	return frappe.call({
		method: "payments.payment_gateways.doctype.braintree_settings.braintree_settings.initiate_payment_session",
		args: {
			"reference_doctype": data.reference_doctype,
			"reference_name": data.reference_docname,
		}
	})
}