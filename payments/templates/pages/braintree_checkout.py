# Copyright (c) 2021, Frappe Technologies Pvt. Ltd. and Contributors
# License: MIT. See LICENSE

import frappe
from frappe import _
from frappe.utils import fmt_money

from payments.payment_gateways.doctype.braintree_settings.braintree_settings import get_gateway_controller

EXPECTED_KEYS = (
	"amount",
	"title",
	"description",
	"reference_doctype",
	"reference_docname",
	"payer_name",
	"payer_email",
	"order_id",
	"currency",
)


def get_context(context):
	context.no_cache = 1

	# all these keys exist in form_dict
	if not set(EXPECTED_KEYS) - set(list(frappe.form_dict)):
		for key in EXPECTED_KEYS:
			context[key] = frappe.form_dict[key]

		gateway_controller = get_gateway_controller(context.reference_doctype, context.reference_docname)
		settings = frappe.get_doc("Braintree Settings", gateway_controller)

		context.formatted_amount = fmt_money(amount=context.amount, currency=context.currency)
		context.locale = frappe.local.lang
		context.header_img = frappe.db.get_value("Braintree Settings", gateway_controller, "header_img")
		context.client_token = settings.generate_token(context)

		context["customer_data"] = {}
		if context.reference_doctype == "Payment Request":
			context["customer_data"] = get_customer_data(context.reference_docname)

	else:
		frappe.redirect_to_message(_("Invalid link"), _("This link is not valid.<br>Please contact us."))
		frappe.local.flags.redirect_location = frappe.local.response.location
		raise frappe.Redirect


@frappe.whitelist(allow_guest=True)
def make_payment(payload_nonce, data):
	data = frappe.parse_json(data)
	data.update({"payload_nonce": payload_nonce})

	gateway_controller = get_gateway_controller(data["reference_doctype"], data["reference_docname"])
	return frappe.get_doc("Braintree Settings", gateway_controller).create_payment_request(data)


def get_customer_data(payment_request):
	pr = frappe.db.get_value("Payment Request", payment_request, ["reference_doctype", "reference_name"], as_dict=True)
	reference = frappe.get_cached_doc(pr.reference_doctype, pr.reference_name)
	contact, address = {}, {}
	if reference.get("contact_person"):
		contact = frappe.get_cached_doc("Contact", reference.contact_person)
	if reference.get("customer_address"):
		address = frappe.get_cached_doc("Address", reference.customer_address)

	output = {}
	output["email"] = contact.get("email_id")
	output["billing-phone"] = contact.get("mobile_no") or contact.get("phone")
	output["billing-surname"] = contact.get("last_name")
	output["billing-given-name"] = contact.get("first_name")

	output["billing-street-address"] = address.get("address_line1")
	output["billing-extended-address"] = address.get("address_line2")
	output["billing-postal-code"] = address.get("pincode")
	output["billing-locality"] = address.get("city")

	return output
