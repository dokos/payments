import frappe
from erpnext import get_default_currency


def get_context(context):
	callback_args = frappe.form_dict

	context.payment_request = callback_args.get("foreign-tx-id")
	context.payment_status = "Pending"

	if callback_args.get("smp-status") == "success":
		context.payment_status = "Paid"
	else:
		context.payment_status = "Failed"
		context.payment_failure = callback_args.get("smp-failure-cause")

	if context.payment_request:
		if transaction_id := callback_args.get("smp-tx-code"):
			frappe.db.set_value("Payment Request", context.payment_request, "transaction_reference", transaction_id)
			frappe.db.commit() # Transaction is a GET

		gateway_controller = frappe.get_single("Sumup Settings")
		transaction_currency = frappe.db.get_value("Payment Request", context.payment_request, "currency") or get_default_currency()
		if payment_gateway := frappe.db.get_value("Payment Gateway", dict(gateway_settings=gateway_controller.doctype, disabled=0)):
			frappe.db.set_value("Payment Request", context.payment_request, "payment_gateway", payment_gateway)
			if payment_gateway_account := frappe.db.get_value(
				"Payment Gateway Account", dict(payment_gateway=payment_gateway, currency=transaction_currency), ["name", "payment_gateway", "payment_channel"], as_dict=1
			):
				frappe.db.set_value("Payment Request", context.payment_request, "payment_gateway_account", payment_gateway_account.name)

		return gateway_controller.run_method("on_payment_success", payment_request_doctype="Payment Request", payment_request=context.payment_request)

	else:
		frappe.local.flags.redirect_location = "/payment-failed?redirect_to=/app/payment-request"
		raise frappe.Redirect
