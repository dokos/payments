# Copyright (c) 2024, Dokos SAS and Contributors
# License: MIT. See LICENSE

import frappe
from frappe import _

from payments.utils.utils import get_gateway_controller

EXPECTED_KEYS = (
	"amount",
	"title",
	"description",
	"reference_doctype",
	"reference_docname",
	"payer_name",
	"payer_email",
	"order_id",
	"currency",
)


def get_context(context):
	context.no_cache = 1

	# all these keys exist in form_dict
	if not set(EXPECTED_KEYS) - set(list(frappe.form_dict)):
		for key in EXPECTED_KEYS:
			context[key] = frappe.form_dict[key]

		gateway_controller = get_gateway_controller(context.reference_doctype, context.reference_docname)
		settings = frappe.get_doc("HelloAsso Settings", gateway_controller)

		context.payment_link = settings.get_payment_intent_link(**frappe.form_dict)

	else:
		frappe.redirect_to_message(_("Invalid link"), _("This link is not valid.<br>Please contact us."))
		frappe.local.flags.redirect_location = frappe.local.response.location
		raise frappe.Redirect
