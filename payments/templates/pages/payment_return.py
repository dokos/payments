# Copyright (c) 2024, Dokos SAS and Contributors
# License: MIT. See LICENSE

import frappe

from payments.utils.utils import get_payment_gateway_controller

no_cache = True


def get_context(context):
	if frappe.local.form_dict.payment_key:
		if payment_request := frappe.db.get_value(
			"Payment Request", {"payment_key": frappe.local.form_dict.payment_key}
		):
			payment_gateway = frappe.db.get_value("Payment Request", payment_request, "payment_gateway")
			gateway_controller = get_payment_gateway_controller(payment_gateway)
			return gateway_controller.run_method(
				"on_payment_success",
				payment_request_doctype="Payment Request",
				payment_request=payment_request,
			)

	elif frappe.local.form_dict.reference_doctype and frappe.local.form_dict.reference_docname:
		payment_gateway = frappe.db.get_value(
			frappe.local.form_dict.reference_doctype,
			frappe.local.form_dict.reference_docname,
			"payment_gateway",
		)
		gateway_controller = get_payment_gateway_controller(payment_gateway)
		return gateway_controller.run_method("on_payment_success", payment_request_doctype=frappe.local.form_dict.reference_doctype, payment_request=frappe.local.form_dict.reference_docname)

	frappe.local.flags.redirect_location = "payment-success"
	raise frappe.Redirect
