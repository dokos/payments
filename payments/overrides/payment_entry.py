import frappe
from erpnext.accounts.doctype.payment_entry.payment_entry import get_payment_entry
from erpnext.controllers.sales_and_purchase_return import make_return_doc
from frappe import _
from frappe.utils import flt, nowdate

from payments.payment_gateways.doctype.stancer_settings.stancer_settings import PAYMENT_STATUSES


@frappe.whitelist()
def can_be_refunded(doc):
	doc = frappe._dict(
		frappe.parse_json(doc)
	)

	if not is_available_for_refund(doc):
		return False

	return True


def is_available_for_refund(doc):
	if doc.payment_type != "Receive":
		False

	if len(doc.references) > 1:
		return False

	if not doc.reference_no and not doc.reference_no.startswith("paym_"):
		return False

	if doc.get("payment_request"):
		if gateway := frappe.db.get_value("Payment Request", doc["payment_request"], "payment_gateway"):
			if frappe.db.get_value("Payment Gateway", gateway, "gateway_settings") == "Stancer Settings":
				controller_name = frappe.db.get_value("Payment Gateway", gateway, "gateway_controller")
				settings = frappe.get_doc("Stancer Settings", controller_name)
				api = settings.get_payments_api()
				payment = api.get_one(doc.reference_no)
				if payment.get("status") in PAYMENT_STATUSES["success"]:
					return True

	return False


@frappe.whitelist()
def refund_payment(doc):
	doc = frappe._dict(
		frappe.parse_json(doc)
	)
	PaymentRefund(doc).refund()



class PaymentRefund:
	def __init__(self, doc):
		self.doc = doc
		self.payment_request = self.doc.get("payment_request")
		self.gateway = frappe.db.get_value("Payment Request", doc["payment_request"], "payment_gateway")
		if not self.gateway:
			frappe.throw(_("A refund can only be triggered if a payment gateway is linked to the payment request."))
		gateway_name = frappe.db.get_value("Payment Gateway", self.gateway, "gateway_controller")
		self.stancer_settings = frappe.get_cached_doc("Stancer Settings", gateway_name)
		self.reference = frappe._dict()
		self.return_doc = frappe._dict()
		self.refund_doc = {}

	def refund(self):
		try:
			if self.doc.references:
				self.validate_refund_prerequisites()
				self.create_return_for_reference()

			self.generate_refund()
			self.generate_payments()
		except Exception:
			frappe.db.rollback()
			frappe.log_error(
				_("Payment Refund Error")
			)


	def validate_refund_prerequisites(self):
		if not is_available_for_refund(self.doc):
			frappe.throw(_("This document is not available for refund."))


	def create_return_for_reference(self):
		self.reference = frappe._dict(self.doc.references[0]) # For now we only allow refund against a single reference
		self.return_doc = make_return_doc(self.reference.reference_doctype, self.reference.reference_name)
		self.return_doc.advances = []
		self.return_doc.flags.no_reconciliation_message_for_returns = True
		self.return_doc.insert()
		self.return_doc.submit()


	def generate_refund(self):
		self.refund_doc = self.stancer_settings.create_refund(
			payment_id=self.doc.reference_no,
			amount=self.doc.total_allocated_amount
		)

		if self.refund_doc.get("error"):
			frappe.throw(title=_("Stancer refund error"), msg=self.refund_doc.get("error"))

	def generate_payments(self):
		if self.doc.references:
			payment_entry = get_payment_entry(
				dt=self.return_doc.doctype,
				dn=self.return_doc.name,
				party_amount=self.doc.total_allocated_amount * -1
			)
			payment_entry.payment_type = "Pay"

			fees = 0.0
			for deduction in self.doc.deductions:
				payment_entry.append("deductions", {
					"account": deduction["account"],
					"cost_center": deduction["cost_center"],
					"amount": deduction["amount"] * -1
				})
				fees += flt(deduction["amount"])

			if fees:
				payment_entry.update(
				{
					"paid_amount": flt(payment_entry.paid_amount) - fees,
					"received_amount": flt(payment_entry.received_amount) - fees,
				}
			)
			payment_entry.payment_request = self.doc.payment_request
			payment_entry.reference_no = self.refund_doc.get("id")
			payment_entry.reference_date = nowdate()
			payment_entry.insert()
			payment_entry.submit()
		else:
			payment_entry = frappe.copy_doc(self.doc)
			payment_entry.payment_type = "Pay"
			payment_entry.paid_amount = payment_entry.paid_amount * -1
			for deduction in payment_entry.deductions:
				deduction["amount"] = deduction["amount"] * 1
			payment_entry.insert()
			payment_entry.submit()

