from frappe import _


def get_dashboard_data(data):
	data["transactions"].append({"label": _("Sepa Mandates"), "items": ["Sepa Mandate"]})

	return data
