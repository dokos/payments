frappe.provide("payments.sumup");
frappe.provide("payments.ui");

frappe.ui.form.on("Payment Request", {
	refresh(frm) {
		if (
			["Requested", "Pending"].includes(frm.doc.status) &&
			frm.doc.payment_gateway &&
			frm.doc.transaction_reference
		) {
			frappe.db.get_value(
				"Payment Gateway",
				frm.doc.payment_gateway,
				"gateway_settings",
				(r) => {
					if (
						["Stancer Settings", "HelloAsso Settings", "Sumup Settings"].includes(
							r.gateway_settings
						)
					) {
						frm.add_custom_button(__("Update payment status"), function () {
							frappe
								.call({
									method: "payments.utils.utils.update_payment_request_status",
									args: {
										payment_request: frm.doc.name,
									},
									freeze: true,
								})
								.then(() => {
									frm.reload_doc();
								});
						});
					}
				}
			);
		}

		if (frm.doc.docstatus == 1 && !["Paid", "Error", "Cancelled"].includes(frm.doc.status) && frappe.is_mobile()) {
			frm.trigger("show_sumup_button")
		}
	},

	async show_sumup_button(frm) {
		const is_sumup_enabled =  await payments.sumup.is_sumup_enabled()
		if (is_sumup_enabled && payments.sumup.supported_currencies.includes(frm.doc.currency)) {
			new payments.ui.SumUpButton({
				total: frm.doc.grand_total,
				currency: frm.doc.currency,
				title: frm.doc.subject,
				mobile_no: "",
				email: frm.doc.email_to,
				reference_name: frm.doc.name,
				reference_doctype: frm.doctype
			});
		}
	}
});
