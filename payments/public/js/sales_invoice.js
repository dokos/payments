frappe.provide("payments.sumup");
frappe.provide("payments.ui");


frappe.ui.form.on("Sales Invoice", {
	refresh(frm) {
		if (frm.doc.docstatus == 1 && frm.doc.outstanding_amount && frappe.is_mobile()) {
			frm.trigger("show_sumup_button")
		}
	},

	async show_sumup_button(frm) {
		const is_sumup_enabled =  await payments.sumup.is_sumup_enabled()
		if (is_sumup_enabled && payments.sumup.supported_currencies.includes(frm.doc.currency)) {
			new payments.ui.SumUpButton({
				total: frm.doc.outstanding_amount,
				currency: frm.doc.currency,
				title: frm.doc.name,
				mobile_no: frm.doc.contact_mobile,
				email: frm.doc.contact_email,
				reference_name: frm.doc.name,
				reference_doctype: frm.doctype
			});
		}
	}
});
