frappe.ui.form.on("Payment Entry", {
	refresh(frm) {
		frm.trigger("add_refund_button");
	},

	add_refund_button(frm) {
		if (frm.doc.payment_request) {
			frappe.call({
				method: "payments.overrides.payment_entry.can_be_refunded",
				args: {
					doc: frm.doc
				}
			}).then(r => {
				if (r.message) {
					frm.page.add_inner_button(__("Refund"), () =>
						frm.trigger("trigger_refund")
					, null, "primary");
				}
			})
		}
	},

	trigger_refund(frm) {
		frappe.show_alert({
			message: __("Refund in progress"),
			indicator: "orange"
		})
		frappe.call({
			method: "payments.overrides.payment_entry.refund_payment",
			args: {
				doc: frm.doc
			}
		}).then(r => {
			if (!r.exc) {
				console.log(r.message)
				frappe.show_alert({
					message: __("Refund completed"),
					indicator: "green"
				})
				frm.refresh()
			}
		})
	}
})