# Copyright (c) 2025, Frappe Technologies and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.query_builder import Interval
from frappe.query_builder.functions import Now


class PaymentSession(Document):
	def before_insert(self):
		self.check_if_session_is_already_successful()

		for existing_session in frappe.get_all("Payment Session", filters={
			"status": "Pending",
			"reference_doctype": self.reference_doctype,
			"reference_name": self.reference_name
		}):
			frappe.db.set_value("Payment Session", existing_session.name, "status", "Closed")


	def check_if_session_is_already_successful(self):
		if frappe.db.exists("Payment Session", {
			"status": "Success",
			"reference_doctype": self.reference_doctype,
			"reference_name": self.reference_name
		}):
			frappe.throw(_("This payment has already been completed successfully."))

	@staticmethod
	def clear_old_logs(days=90):
		table = frappe.qb.DocType("Payment Session")
		frappe.db.delete(table, filters=(table.modified < (Now() - Interval(days=days))))

