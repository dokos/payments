import frappe


class GoCardlessCustomers:
	def __init__(self, gateway):
		self.gateway = gateway
		self.client = self.gateway.client

	# Restricted to GoCardless Pro
	def create(self, **kwargs):
		return self.client.customers.create(params=kwargs)

	def get(self, id):
		return self.client.customers.get(id)

	def update(self, id, **kwargs):
		return self.client.customers.update(id, params=kwargs)

	def remove(self, id):
		return self.client.customers.remove(
			id,
		)

	def get_list(self, **kwargs):
		return self.client.customers.list(params=kwargs)

	def register(self, gocardless_id, customer):
		try:
			customer = frappe.get_cached_doc("Customer", customer)
			payment_gateway = frappe.db.get_value(
				"Payment Gateway",
				dict(
					gateway_settings="GoCardless Settings", gateway_controller=self.gateway.name, disabled=0
				),
			)
			if row_exists := customer.get(
				"payment_gateways_references", dict(payment_gateway=payment_gateway)
			):
				if row_exists[0].customer_id == gocardless_id:
					return
				row_exists[0].customer_id = gocardless_id
			else:
				customer.append(
					"payment_gateways_references",
					{"payment_gateway": payment_gateway, "customer_id": gocardless_id},
				)
			customer.flags.ignore_permissions = True
			customer.save()
		except Exception as e:
			frappe.log_error(e, "GoCardless Customer ID Registration Error")
