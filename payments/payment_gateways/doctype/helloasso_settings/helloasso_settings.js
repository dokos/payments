// Copyright (c) 2024, Frappe Technologies and contributors
// For license information, please see license.txt

frappe.ui.form.on("HelloAsso Settings", {
  refresh(frm) {
    frm.trigger("show_headline");
  },

  show_headline(frm) {
    frm.dashboard.clear_headline();
    frm.dashboard.set_headline(`
			<img src="https://api.helloasso.com/v5/img/logo-ha.svg" alt="" style="padding: 0.2rem; height: 2.25rem;"/>
			<span style="font-size: 1rem; font-weight: 500;">HelloAsso</span>
			<div>
				HelloAsso aide les associations à collecter des paiements en ligne et propose ses services gratuitement.<br>
				Elle prend à sa charge tous les frais de transaction pour que vous puissiez bénéficier de la totalité des sommes versées par vos publics, sans frais.<br>
				Les contributions volontaires laissées par ces derniers sont leur unique source de revenus.
			</div>
		`);
  },
});
