from typing import TYPE_CHECKING

import frappe
from frappe import _
from frappe.integrations.utils import (
	make_delete_request,
	make_get_request,
	make_patch_request,
	make_post_request,
)

if TYPE_CHECKING:
	from payments.payment_gateways.doctype.helloasso_settings.helloasso_settings import HelloAssoSettings

SANDBOX_URI = "https://api.helloasso-sandbox.com"
BASE_URI = "https://api.helloasso.com"


class HelloAssoAPI:
	def __init__(self, gateway):
		self.gateway: HelloAssoSettings = gateway

		if not self.gateway.connected_application:
			frappe.throw(_("Please save save your HelloAsso settings again"))

		self.base = f"{SANDBOX_URI}/v5/" if self.gateway.use_sandbox else f"{BASE_URI}/v5/"
		self.oauth = frappe.get_doc("Connected App", self.gateway.connected_application)

		token = self.oauth.get_active_token(user="")
		if not token:
			token = self.gateway.get_access_token()

		if token:
			self.headers = {
				"Authorization": f"Bearer {token.get_password('access_token')}"
			}
		else:
			frappe.redirect_to_message(_("Invalid link"), _("This link is not valid.<br>Please contact us."))
			frappe.local.flags.redirect_location = frappe.local.response.location
			raise frappe.Redirect

	def get(self, url, params={}):
		return make_get_request(url=f"{self.base}{url}", headers=self.headers, params=params)

	def post(self, url, data={}):
		return make_post_request(url=f"{self.base}{url}", headers=self.headers, json=data)

	def patch(self, url, data={}):
		return make_patch_request(url=f"{self.base}{url}", headers=self.headers, json=data)

	def delete(self, url):
		# We can't use make_delete_request because of a JSONDecodeError with the response
		return make_delete_request(
			url=f"{self.base}{url}",
			headers=self.headers,
		)

	def get_list(self, url, params={}):
		return self.get(url, params)

	def create(self, data):
		return self.post(data)

	def update(self, data):
		return self.patch(data)
