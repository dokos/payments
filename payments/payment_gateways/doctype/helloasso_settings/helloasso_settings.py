# Copyright (c) 2024, Frappe Technologies and contributors
# For license information, please see license.txt

import re
from urllib.parse import quote_plus

import frappe
from frappe import _
from frappe.utils import call_hook_method, flt, get_url, urlencode, validate_email_address

from payments.payment_gateways.doctype.helloasso_settings.api import BASE_URI, SANDBOX_URI, HelloAssoAPI
from payments.utils import create_payment_gateway
from payments.utils.utils import PaymentGatewayController

PAYMENT_STATUSES = {
	"pending": [
		"Pending",
		"Unknown",
		"Registered",
		"Refunding",
		"Waiting",
		"Contested",
		"WaitingBankValidation",
		"WaitingBankWithdraw",
		"WaitingAuthentication",
		"Init",
	],
	"success": ["Authorized", "AuthorizedPreprod", "Corrected", "NoDonation"],
	"failure": ["Refused", "Error", "Refunded", "Canceled", "Abandoned", "Inconsistent"],
	"technical": ["Deleted"],
}

WEBHOOK_ENDPOINT = "/api/method/payments.payment_gateways.doctype.helloasso_settings.webhooks?account="


class HelloAssoSettings(PaymentGatewayController):
	supported_currencies = ["EUR"]

	def validate(self):
		self.callback_url = get_url(f"{WEBHOOK_ENDPOINT}{self.name}")

	def get_access_token(self):
		if not (self.client_id and self.client_secret):
			frappe.throw(_("Please register your client ID and client Secret"))

		connected_app = self.get_update_connected_app()

		token = connected_app.get_backend_app_token()

		return token

	def get_update_connected_app(self):
		connected_app = (
			frappe.get_doc("Connected App", self.connected_application)
			if self.connected_application
			else frappe.new_doc("Connected App")
		)

		base_uri = SANDBOX_URI if self.use_sandbox else BASE_URI

		parameters = {
			"provider_name": "HelloAsso",
			"client_id": self.client_id,
			"client_secret": self.get_password("client_secret", raise_exception=False),
			"scopes": [],
			"authorization_uri": f"{base_uri}/oauth2/token",
			"token_uri": f"{base_uri}/oauth2/token",
		}

		connected_app.update(parameters)
		if not connected_app.query_parameters:
			connected_app.append("query_parameters", {"key": "grant_type", "value": "client_credentials"})

		connected_app.flags.ignore_permissions = True
		connected_app.save()

		if not self.connected_application:
			self.db_set("connected_application", connected_app.name)

		return connected_app

	def get_client(self):
		return HelloAssoAPI(self)

	def on_update(self):
		self.get_access_token()

		create_payment_gateway(
			"HelloAsso-" + self.gateway_name, settings="HelloAsso Settings", controller=self.gateway_name
		)
		call_hook_method("payment_gateway_enabled", gateway="HelloAsso-" + self.gateway_name)

		self.get_organization_slug()

	def get_organization_slug(self):
		client = self.get_client()
		if organizations := client.get("users/me/organizations"):
			self.db_set("organization_slug", organizations[0].get("organizationSlug"))

	def validate_transaction_currency(self, currency):
		if currency not in self.supported_currencies:
			frappe.throw(
				_(
					"Please select another payment method. HelloAsso does not support transactions in currency '{0}'"
				).format(currency)
			)

	def get_payment_url(self, **kwargs):
		data = kwargs
		return get_url(
			"./helloasso_checkout?{0}".format(urlencode(data))
		)

	def get_payment_intent_link(self, **kwargs):
		data = kwargs
		payment_intent = self.create_payment_intent(**data)

		if payment_intent and payment_intent.get("redirectUrl"):
			return payment_intent.get("redirectUrl")
		else:
			return get_url("payment-failed")

	def create_payment_intent(self, **kwargs):
		if not self.organization_slug:
			frappe.throw(_("The configuration of the HelloAsso checkout is not completed. This payment request can only be generated after its completion."))

		client = self.get_client()
		back_url = get_url(uri=frappe.get_request_header("REQUEST_URI", ""))
		if not get_url(uri=frappe.get_request_header("REQUEST_URI", "")).startswith("https://"):
			frappe.throw(_("HelloAsso checkout can only be used on a secure HTTPS site."))

		url = f"organizations/{self.organization_slug}/checkout-intents"
		default_return_url = f"payment-return?reference_doctype={quote_plus(kwargs.get('reference_doctype'))}&reference_docname={quote_plus(kwargs.get('reference_docname'))}"
		params = {
			"totalAmount": round(flt(kwargs.get("amount")) * 100),
			"initialAmount": round(flt(kwargs.get("amount")) * 100),
			"itemName": kwargs.get("description"),
			"backUrl": back_url,
			"errorUrl": get_url("payment-failed"),
			"returnUrl": get_url(
				f"payment-return?payment_key={kwargs.get('payment_key')}"
				if kwargs.get("payment_key")
				else default_return_url
			),
			"containsDonation": False,  # TODO: Add option to HelloAsso Settings and webforms
			"payer": self.get_payer(**kwargs),
			"metadata": {
				"reference_docname": kwargs.get("reference_docname"),
				"reference_doctype": kwargs.get("reference_doctype"),
			},
		}
		if payment_intent := client.post(url, params):
			if frappe.db.has_column(kwargs.get("reference_doctype"), "transaction_reference"):
				frappe.db.set_value(kwargs["reference_doctype"], kwargs["reference_docname"], "transaction_reference", payment_intent.get("id"))

			# For compatibility with web forms
			if frappe.db.has_column(kwargs.get("reference_doctype"), "reference_no"):
				frappe.db.set_value(kwargs["reference_doctype"], kwargs["reference_docname"], "reference_no", payment_intent.get("id"))

			self.payment_session = self.get_or_create_payment_session(
				reference_doctype=kwargs.get("reference_doctype"),
				reference_docname=kwargs.get("reference_docname"),
				reference=payment_intent.get("id")
			)
			self.payment_session.db_set("data", str(payment_intent))

			return payment_intent

	def get_payer(self, **kwargs):
		if kwargs.get("reference_doctype") == "Payment Request":
			payment_request = frappe.db.get_value(
				"Payment Request",
				kwargs.get("reference_docname"),
				["reference_doctype", "reference_name", "customer"],
				as_dict=True,
			)
			if payment_request.reference_doctype in ["Sales Order", "Sales Invoice"]:
				reference_doc_contact = frappe.db.get_value(
					payment_request.reference_doctype, payment_request.reference_name, "contact_person"
				)
				reference_doc_address = frappe.db.get_value(
					payment_request.reference_doctype, payment_request.reference_name, "customer_address"
				)
				contact = frappe.db.get_value(
					"Contact", reference_doc_contact, ["first_name", "last_name", "email_id"], as_dict=True
				)
				address = frappe.db.get_value(
					"Address",
					reference_doc_address,
					["address_line1", "city", "pincode", "country"],
					as_dict=True,
				)

			return {
				"firstName": validate_first_last_name(contact.first_name),
				"lastName": validate_first_last_name(contact.last_name),
				"email": validate_email_address(contact.email_id),
				"dateOfBirth": None,
				"address": address.address_line1,
				"city": address.city,
				"zipCode": address.pincode,
				"country": get_iso_3166_1_country_code(address.country),
				"companyName": payment_request.customer,
			}

		return {}

	def on_payment_success(self, payment_request_doctype: str, payment_request: str):
		payment_request_doc = frappe.get_doc(payment_request_doctype, payment_request)
		payment = {}
		payment_intent_id = payment_request_doc.get("transaction_reference")
		if not payment_intent_id and (frappe.form_dict and frappe.form_dict.checkoutIntentId):
			payment_request_doc.db_set("transaction_reference", frappe.form_dict.checkoutIntentId)
			payment_intent_id = frappe.form_dict.checkoutIntentId

		if payment_intent_id:
			payment = self.get_processed_payment(payment_intent_id)

		status = "Pending"
		reference_no = None
		if payment:
			if payment.get("state") in PAYMENT_STATUSES["success"]:
				status = "Paid"
			elif payment.get("state") in PAYMENT_STATUSES["failure"]:
				status = "Failed"

			reference_no = payment.get("id")

		try:
			self.update_payment_session(payment_request_doc, status, reference_no, payment)
			payment_request_doc.run_method("on_payment_authorized", status=status, reference_no=reference_no)
			frappe.db.commit() # To fix: Can currently be called from a GET method
		except Exception:
			frappe.log_error("HelloAsso payment entry creation error")
			frappe.db.commit()

		frappe.local.flags.redirect_location = "payment-success" if status == "Paid" else "payment-failed"
		raise frappe.Redirect

	def get_processed_payment(self, ref: str):
		if not isinstance(ref, str):
			return {}

		client = self.get_client()
		url = f"organizations/{self.organization_slug}/checkout-intents/{ref}"
		payment_intent = client.get(url)

		if payments := payment_intent.get("order", {}).get("payments"):
			return next(iter(payments))


def validate_first_last_name(txt):
	if len(txt) == 1:
		return

	if any(char.isdigit() for char in txt):
		return

	if re.search(r"(.)\1\1", txt):
		return

	if not [char for char in txt if char in ("a", "e", "i", "o", "u")]:
		return

	if txt in [
		"firstname",
		"lastname",
		"unknown",
		"first_name",
		"last_name",
		"anonyme",
		"user",
		"admin",
		"name",
		"nom",
		"prénom",
		"test",
	]:
		return

	allowed_characters = r"^[A-Za-zÀ-ÖØ-öø-ÿ\'-]*$"
	if not bool(re.search(allowed_characters, txt)):
		return

	return txt


def get_iso_3166_1_country_code(country):
	import pycountry

	country_code = frappe.db.get_value("Country", country, "code")

	return pycountry.countries.get(alpha_3=country_code)


def handle_webhooks(**kwargs):
	integration_request = frappe.get_doc(kwargs.get("doctype"), kwargs.get("docname"))
	integration_request.handle_failure({"message": _("This type of event is not handled")}, "Not Handled")
