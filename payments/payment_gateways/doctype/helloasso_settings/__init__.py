# Copyright (c) 2018, Frappe Technologies and contributors
# For license information, please see license.txt


import json
from urllib.parse import parse_qs

import frappe
from frappe import _


@frappe.whitelist(allow_guest=True)
def webhooks():
	r = frappe.request
	if not r:
		return

	account, events = get_events(r)
	for event in events:
		try:
			doc = create_new_integration_log(event, account)

			frappe.enqueue(
				method="payments.payment_gateways.doctype.helloasso_settings.helloasso_settings.handle_webhooks",
				queue="long",
				timeout=600,
				is_async=True,
				**{"doctype": "Integration Request", "docname": doc.name},
			)
		except Exception:
			frappe.log_error(_("HelloAsso webhooks processing error"))

	frappe.response.message = "Webhook received and event type handled"
	frappe.response.http_status_code = 200


def get_events(request):
	account = get_account(request)
	payload = request.get_data()
	return account, payload


def get_account(request):
	account = None
	if request.query_string:
		parsed_qs = parse_qs(frappe.safe_decode(request.query_string))
		account = parsed_qs.get("account")

		if isinstance(account, list):
			account = account[0]

	if account:
		return account
	else:
		helloasso_accounts = frappe.get_all("HelloAsso Settings")
		if len(helloasso_accounts) > 1:
			frappe.log_error(
				message=_("Please define your HelloAsso account in the webhook URL's query string"),
				title=_("HelloAsso webhook error"),
			)
		else:
			return helloasso_accounts[0].get("name")


def create_new_integration_log(event, account):
	integration_request = frappe.get_doc(
		{
			"doctype": "Integration Request",
			"request_description": "Webhook",
			"integration_request_service": "HelloAsso",
			"service_document": event.attributes.get("resource_type"),
			"service_status": event.attributes.get("action"),
			"service_id": event.attributes.get("id"),
			"data": json.dumps(event.attributes, indent=4),
			"payment_gateway_controller": account,
		}
	)

	integration_request.flags._name = event.attributes.get("id")

	integration_request.insert(ignore_permissions=True, ignore_if_duplicate=True)
	frappe.db.commit()

	return integration_request
