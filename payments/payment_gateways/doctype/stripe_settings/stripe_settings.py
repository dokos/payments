# Copyright (c) 2019, Dokos SAS and contributors
# For license information, please see license.txt

from urllib.parse import quote_plus, urlencode

import frappe
import stripe
from frappe import _
from frappe.utils import call_hook_method, check_format, cint, flt, get_url

from payments.payment_gateways.doctype.stripe_settings.api import (
	StripeCustomer,
	StripeInvoiceItem,
	StripePaymentIntent,
	StripePrice,
	StripeWebhookEndpoint,
)
from payments.payment_gateways.doctype.stripe_settings.stripe_data_handler import StripeDataHandler
from payments.payment_gateways.doctype.stripe_settings.webhook_events import (
	StripeSetupWebhooksController,
	StripeWebhooksController,
)
from payments.utils import create_payment_gateway
from payments.utils.utils import PaymentGatewayController

WEBHOOK_ENDPOINT = (
	"/api/method/payments.payment_gateways.doctype.stripe_settings.webhooks?account="
)


PAYMENT_STATUSES = {
	"success": ["succeeded"],
	"pending": ["processing", "requires_action", "requires_capture", "requires_confirmation", "requires_payment_method"],
	"failure": ["canceled"]
}


class StripeSettings(PaymentGatewayController):
	currency_wise_minimum_charge_amount = {  # noqa: RUF012
		"JPY": 50,
		"MXN": 10,
		"DKK": 2.50,
		"HKD": 4.00,
		"NOK": 3.00,
		"SEK": 3.00,
		"USD": 0.50,
		"AUD": 0.50,
		"BRL": 0.50,
		"CAD": 0.50,
		"CHF": 0.50,
		"EUR": 0.50,
		"GBP": 0.30,
		"NZD": 0.50,
		"SGD": 0.50,
	}

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		if not self.is_new():
			self.configure_stripe()

	def before_insert(self):
		self.gateway_name = frappe.scrub(self.gateway_name)

	def validate(self):
		if self.name:
			self.webhook_url = f"{frappe.utils.get_url(WEBHOOK_ENDPOINT)}{self.name}"

	@property
	def stripe(self):
		"""stripe is moved to property because it would be pickled by frappe.get_cached_doc"""
		return stripe

	def get_secret_key(self):
		return self.get_password("secret_key", raise_exception=False)

	def configure_stripe(self):
		self.stripe.api_key = self.get_secret_key()
		self.stripe.default_http_client = stripe.http_client.RequestsClient()

	def get_supported_currencies(self):
		account = self.stripe.Account.retrieve()
		supported_payment_currencies = self.stripe.CountrySpec.retrieve(account["country"])[
			"supported_payment_currencies"
		]

		return [currency.upper() for currency in supported_payment_currencies]

	def get_supported_payment_methods(self):
		payment_methods = []
		for field in ["card", "sepa_debit"]:
			if cint(self.get(field)):
				payment_methods.append(field)

		return payment_methods

	def on_update(self):
		create_payment_gateway(
			"Stripe-" + self.gateway_name, settings="Stripe Settings", controller=self.gateway_name
		)
		call_hook_method("payment_gateway_enabled", gateway="Stripe-" + self.gateway_name)
		if not self.flags.ignore_mandatory:
			self.validate_stripe_credentials()

	def validate_stripe_credentials(self):
		try:
			self.configure_stripe()
			balance = self.stripe.Balance.retrieve()
			return balance
		except Exception as e:
			frappe.throw(_("Stripe connection could not be initialized.<br>Error: {0}").format(str(e)))

	def validate_transaction_currency(self, currency):
		if currency not in self.get_supported_currencies():
			frappe.throw(
				_(
					"Please select another payment method. Stripe does not support transactions in currency '{0}'"
				).format(currency)
			)

	def validate_minimum_transaction_amount(self, currency, amount):
		if currency in self.currency_wise_minimum_charge_amount:
			if flt(amount) < self.currency_wise_minimum_charge_amount.get(currency, 0.0):
				frappe.throw(
					_("For currency {0}, the minimum transaction amount should be {1}").format(
						currency, self.currency_wise_minimum_charge_amount.get(currency, 0.0)
					)
				)

	def get_stripe_plan(self, plan, currency):
		try:
			stripe_plan = StripePrice(self).retrieve(plan)
			if not stripe_plan.active:
				frappe.throw(_("Payment plan {0} is no longer active.").format(plan))
			if not currency == stripe_plan.currency.upper():
				frappe.throw(
					_("Payment plan {0} is in currency {1}, not {2}.").format(
						plan, stripe_plan.currency.upper(), currency
					)
				)
			return stripe_plan
		except stripe.error.InvalidRequestError:
			frappe.throw(_("Invalid Stripe plan or currency: {0} - {1}").format(plan, currency))

	def get_stripe_invoice_item(self, item, currency):
		try:
			invoice_item = StripeInvoiceItem(self).retrieve(item)
			if not currency == invoice_item.currency.upper():
				frappe.throw(
					_("Payment plan {0} is in currency {1}, not {2}.").format(
						item, invoice_item.currency.upper(), currency
					)
				)
			return invoice_item
		except stripe.error.InvalidRequestError:
			frappe.throw(_("Invalid currency for invoice item: {0} - {1}").format(item, currency))

	def get_stripe_mode_from_payment_type(self, payment_type):
		if payment_type == "immediate":
			return "payment"
		elif payment_type == "offline":
			return "setup"
		elif payment_type == "immediate+offline":
			return "payment+setup"
		else:
			frappe.throw(_("Invalid payment type: {0}").format(payment_type))

	def get_payment_url(self, **kwargs):
		data = kwargs
		if not data.get("_legacy", None):
			try:
				if payment_type := data.get("_payment_type"):
					data["mode"] = self.get_stripe_mode_from_payment_type(payment_type)
					del data["_payment_type"]
				data = StripeDataHandler.generate_query_params(self, data)
			except Exception:
				frappe.log_error(
					"Warning: Stripe URL generation failure - Falling back to legacy method",
					frappe.get_traceback(),
				)

		return get_url(f"./stripe_checkout?{urlencode(data)}")

	def cancel_subscription(self, **kwargs):
		from payments.payment_gateways.doctype.stripe_settings.api import StripeSubscription

		return StripeSubscription(self).cancel(
			kwargs.get("subscription"),
			invoice_now=kwargs.get("invoice_now", False),
			prorate=kwargs.get("prorate", False),
		)

	def get_stripe_customer_id(self, customer):
		if payment_gateway := frappe.db.get_value(
			"Payment Gateway",
			dict(gateway_settings="Stripe Settings", gateway_controller=self.name, disabled=0),
		):
			return frappe.db.get_value(
				"Payment Gateways References",
				dict(
					parenttype="Customer",
					parent=customer,
					payment_gateway=payment_gateway,
				),
				"customer_id",
			)

	def immediate_payment_processing(
		self, reference, customer, amount, currency, description, metadata
	):
		try:
			stripe_customer_id = self.get_stripe_customer_id(customer)
			payment_intent_id = self.create_payment_intent(
				reference, stripe_customer_id, amount, currency, description, metadata
			)
			return payment_intent_id
		except Exception:
			frappe.log_error(
				_("Stripe direct processing failed for {0}").format(reference),
				message=frappe.get_traceback(),
				reference_doctype=isinstance(metadata, dict) and metadata.get("reference_doctype"),
				reference_name=isinstance(metadata, dict) and metadata.get("reference_name"),
			)

	def create_payment_intent(self, reference, customer, amount, currency, description, metadata):
		payment_method = (
			StripeCustomer(self).get(customer).get("invoice_settings", {}).get("default_payment_method")
		)

		payment_intent = (
			StripePaymentIntent(self, reference).create(
				amount=round(flt(amount) * 100.0),
				description=description,
				currency=currency,
				customer=customer,
				confirm=True,
				off_session=True,
				metadata=metadata,
				payment_method=payment_method,
				payment_method_types=self.get_supported_payment_methods(),
			)
			or {}
		)

		self.trigger_on_payment_authorized(metadata, payment_intent.get("id"))
		self.register_payment_session(
			metadata.get("reference_doctype"),
			metadata.get("reference_name") or metadata.get("reference_docname"),
			payment_intent
		)

		return payment_intent.get("id")

	def make_line_item(self, amount, currency, description):
		return {
			"price_data": {
				"currency": currency,
				"product_data": {
					"name": description,
				},
				"unit_amount": round(flt(amount) * 100.0),
			},
			"quantity": 1,
		}

	def get_base_options(self, customer, redirect_urls, metadata, payment_method_types=None):
		if not payment_method_types:
			payment_method_types = self.get_supported_payment_methods()

		return_url = f"/payment-return?reference_doctype={quote_plus(metadata.get('reference_doctype'))}&reference_docname={quote_plus(metadata.get('reference_name'))}"
		return dict(
			customer=customer,
			metadata=metadata,
			payment_method_types=payment_method_types,
			success_url=get_url(return_url),
			cancel_url=get_url(redirect_urls["cancel"]),
		)

	def get_payment_options(
		self,
		*,
		customer,
		customer_email,
		item,
		redirect_urls,
		metadata,
		also_setup_future_usage=False,
		payment_method_types=None,
	):
		payment_intent_data = {"metadata": metadata}
		more_options = {}

		if also_setup_future_usage:
			payment_intent_data["setup_future_usage"] = "off_session"
			# more_options["customer_creation"] = "required"
			# more_options["consent_collection"] = { "terms_of_service": "required" }
			custom_text = _("This payment method will be used for subsequent recurring payments.")
			more_options["custom_text"] = {"submit": {"message": custom_text}}
			if not customer:
				raise ValueError("The `customer` parameter is required with also_setup_future_usage=True")
		else:
			if not customer and check_format(customer_email):
				more_options["customer_email"] = customer_email

		base_options = self.get_base_options(customer, redirect_urls, metadata, payment_method_types)

		return dict(
			**base_options,
			**dict(
				mode="payment",
				line_items=[self.make_line_item(**item)],
				payment_intent_data=payment_intent_data,
				**more_options,
			),
		)

	def get_setup_options(
		self,
		*,
		customer,
		redirect_urls,
		metadata,
		also_setup_future_usage=False,
		payment_method_types=None,
	):
		base_options = self.get_base_options(customer, redirect_urls, metadata, payment_method_types)

		return dict(**base_options, **dict(mode="setup", setup_intent_data={"metadata": metadata}))

	def create_payment_checkout_session(self, **kwargs):
		options = self.get_payment_options(**kwargs)
		checkout_session = stripe.checkout.Session.create(**options)

		# NOTE: A PaymentIntent is no longer created during Checkout Session creation in payment mode.
		# https://stripe.com/docs/upgrades#2022-08-01
		self.trigger_on_payment_authorized(options["metadata"], payment_intent=checkout_session.get("id"))

		metadata = options["metadata"]
		self.register_payment_session(
				metadata.get("reference_doctype"),
				metadata.get("reference_name") or metadata.get("reference_docname"),
				checkout_session
			)

		return checkout_session

	def create_setup_checkout_session(self, **kwargs):
		options = self.get_setup_options(**kwargs)
		checkout_session = stripe.checkout.Session.create(**options)

		metadata = options["metadata"]
		if checkout_session.get("id") and metadata.get("reference_doctype") and (
			metadata.get("reference_name") or metadata.get("reference_docname")
		):
			reference_name =(metadata.get("reference_name") or metadata.get("reference_docname"))
			self.register_transaction_reference(
				metadata.get("reference_doctype"),
				reference_name,
				checkout_session.get("id")
			)
			self.register_payment_session(
				metadata.get("reference_doctype"),
				reference_name,
				checkout_session
			)
		return checkout_session

	def trigger_on_payment_authorized(self, metadata, payment_intent=None):
		if metadata.get("reference_doctype") and (
			metadata.get("reference_name") or metadata.get("reference_docname")
		):
			reference_document = frappe.get_doc(
				metadata.get("reference_doctype"),
				(metadata.get("reference_name") or metadata.get("reference_docname")),
			)
			if payment_intent:
				self.register_transaction_reference(
					reference_document.doctype,
					reference_document.name,
					payment_intent
				)

			reference_document.run_method("on_payment_authorized", "Pending", payment_intent)

	def register_transaction_reference(self, reference_dt, reference_name, payment_intent):
		if frappe.db.has_column(reference_dt, "transaction_reference"):
			frappe.db.set_value(reference_dt, reference_name, "transaction_reference", payment_intent)

		# For compatibility with web forms
		if frappe.db.has_column(reference_dt, "reference_no"):
			frappe.db.set_value(reference_dt, reference_name, "reference_no", payment_intent)

		frappe.db.commit() # Can be called from a GET request

	def register_payment_session(self, reference_doctype, reference_name, reference):
		self.payment_session = self.get_or_create_payment_session(
			reference_doctype=reference_doctype,
			reference_docname=reference_name,
			reference=reference.get("id")
		)
		self.payment_session.db_set("data", str(reference))

		frappe.db.commit() # Can be called from a GET request

	def get_transaction_fees(self, payment_intent):
		stripe_payment_intent_object = self.stripe.PaymentIntent.retrieve(
			payment_intent, expand=["latest_charge.balance_transaction"]
		)
		return frappe._dict(
			base_amount=flt(stripe_payment_intent_object.latest_charge.amount) / 100.0,
			fee_amount=flt(stripe_payment_intent_object.latest_charge.balance_transaction.fee) / 100.0,
			exchange_rate=flt(stripe_payment_intent_object.latest_charge.balance_transaction.exchange_rate),
		)

	def get_customer_id(self, payment_intent):
		stripe_payment_intent_object = self.stripe.PaymentIntent.retrieve(payment_intent)
		if stripe_payment_intent_object:
			return stripe_payment_intent_object.customer
		else:
			return None

	def get_processed_payment_intent(self, reference_no):
		payment_intent_doc = None
		if reference_no.startswith("pi_"):
			payment_intent_doc = self.stripe.PaymentIntent.retrieve(reference_no, expand=['payment_method'])
		elif reference_no.startswith("cs_"):
			checkout_session = self.stripe.checkout.Session.retrieve(reference_no)
			payment_intent_id = checkout_session.get("payment_intent")
			setup_intent_id = checkout_session.get("setup_intent")
			if not payment_intent_id and not setup_intent_id:
				frappe.local.flags.redirect_location = self.failure_redirect_url or "payment-failed"
				raise frappe.Redirect
			if setup_intent_id:
				payment_intent_doc = self.stripe.SetupIntent.retrieve(setup_intent_id, expand=['payment_method'])
			elif payment_intent_id:
				payment_intent_doc = self.stripe.PaymentIntent.retrieve(payment_intent_id, expand=['payment_method'])

		return payment_intent_doc

	def on_payment_success(self, payment_request_doctype: str, payment_request: str): #TODO: Commonify
		payment_request_doc = frappe.get_doc(payment_request_doctype, payment_request)
		if payment_request_doc.get("status") == "Paid":
			frappe.local.flags.redirect_location = self.redirect_url or "payment-success"
			raise frappe.Redirect

		payment_intent = self.get_payment_intent_from_reference(
			payment_request_doc.get("transaction_reference") or payment_request_doc.get("reference_no")
		)

		status = "Failed"
		reference_no = None
		if payment_intent:
			status = self.get_payment_intent_status(payment_intent)
			reference_no = payment_intent.get("id")
			if payment_intent.get("customer") and payment_intent.get("payment_method", {}).get("id") and payment_intent.get("payment_method", {}).get("type"):
				self.register_customer(payment_intent.get("customer"), payment_intent.get("payment_method", {}).get("type"), payment_intent.get("payment_method", {}).get("id"), payment_request_doc.customer)
				frappe.db.commit() # To fix: Can currently be called from a GET method

		if not reference_no and payment_request_doc.get("transaction_reference"):
			reference_no = payment_request_doc.transaction_reference

		elif not reference_no and payment_request_doc.get("reference_no"):
			reference_no = payment_request_doc.reference_no

		# payment_request_doc.run_method("on_payment_authorized", status=status, reference_no=reference_no)

		if status == "Paid":
			frappe.local.flags.redirect_location = self.redirect_url or "payment-success"
		elif status == "Pending":
			self.on_payment_success(payment_request_doctype, payment_request)
		else:
			frappe.local.flags.redirect_location = self.failure_redirect_url or "payment-failed"

		raise frappe.Redirect


	def get_payment_intent_from_reference(self, reference=None):
		return self.get_processed_payment_intent(reference) if reference else {}

	def get_payment_intent_status(self, payment_intent):
		status = "Pending"
		if payment_intent.get("status") in PAYMENT_STATUSES["success"]:
			status = "Paid"
		elif payment_intent.get("status") in PAYMENT_STATUSES["failure"]:
			status = "Failed"

		return status

	def register_customer(self, stripe_customer, payment_method, payment_method_id, customer):
		payment_gateway = frappe.db.get_value(
			"Payment Gateway",
			dict(gateway_settings="Stripe Settings", gateway_controller=self.name, disabled=0),
		)

		for row in frappe.get_all("Payment Gateways References", filters={
			"parenttype": "Customer",
			"parent": customer,
			"payment_gateway": payment_gateway,
		}, fields=["customer_id", "payment_method", "payment_method_id", "name"]):
			if not (
				row.customer_id == stripe_customer
				and row.payment_method == payment_method
				and row.payment_method_id == payment_method_id
			):
				frappe.db.set_value("Payment Gateways References", row.name, "customer_id", stripe_customer)
				frappe.db.set_value("Payment Gateways References", row.name, "payment_method", payment_method)
				frappe.db.set_value("Payment Gateways References", row.name, "payment_method_id", payment_method_id)
		else:
			new_reference = frappe.new_doc("Payment Gateways References")
			new_reference.parenttype = "Customer"
			new_reference.parent = customer
			new_reference.update({
				"payment_gateway": payment_gateway,
				"customer_id": stripe_customer,
				"payment_method": payment_method,
				"payment_method_id": payment_method_id,
			})
			new_reference.insert(ignore_permissions=True)

		if not frappe.db.get_value("Customer", customer, "mode_of_payment"):
			frappe.db.set_value("Customer", customer, "mode_of_payment", payment_method)


def handle_webhooks(**kwargs):
	integration_request = frappe.get_doc(kwargs.get("doctype"), kwargs.get("docname"))

	if integration_request.service_document in ["charge", "payment_intent", "invoice", "checkout"]:
		StripeWebhooksController(**kwargs)
	elif integration_request.service_document in ["setup_intent"]:
		StripeSetupWebhooksController(**kwargs)
	else:
		integration_request.handle_failure(
			{"message": _("This type of event is not handled")}, "Not Handled"
		)


@frappe.whitelist()
def create_delete_webhooks(settings, action="create"):
	stripe_settings = frappe.get_doc("Stripe Settings", settings)
	url = f"{frappe.utils.get_url(WEBHOOK_ENDPOINT)}{stripe_settings.name}"

	if action == "create":
		return create_webhooks(stripe_settings, url)
	elif action == "delete":
		return delete_webhooks(stripe_settings, url)


def create_webhooks(stripe_settings, url):
	webhook_controllers: list = [
		StripeWebhooksController,
		StripeSetupWebhooksController,
	]
	enabled_events: set[str] = set()
	for controller_cls in webhook_controllers:
		# TODO: Maybe throw if duplicate events are added
		enabled_events.update(controller_cls.get_handled_events())
	enabled_events = list(sorted(enabled_events))

	try:
		result = StripeWebhookEndpoint(stripe_settings).create(url, enabled_events)
		if result:
			frappe.db.set_value(
				"Stripe Settings", stripe_settings.name, "webhook_secret_key", result.get("secret")
			)
		return result
	except Exception:
		frappe.log_error(_("Stripe webhook creation error"))


def delete_webhooks(stripe_settings, url):
	webhooks_list = StripeWebhookEndpoint(stripe_settings).get_all()

	for webhook in webhooks_list.get("data", []):
		if webhook.get("url") == url:
			try:
				StripeWebhookEndpoint(stripe_settings).delete(webhook.get("id"))
				frappe.db.set_value("Stripe Settings", stripe_settings.name, "webhook_secret_key", "")
			except Exception:
				frappe.log_error(_("Stripe webhook deletion error"))

	return webhooks_list
