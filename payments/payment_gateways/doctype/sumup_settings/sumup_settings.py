# Copyright (c) 2024, Frappe Technologies and contributors
# For license information, please see license.txt

from urllib.parse import urlencode

import frappe
from erpnext.accounts.doctype.payment_request.payment_request import (
	get_existing_payment_request_amount,
	make_payment_request,
)
from frappe import _
from frappe.custom.doctype.custom_field.custom_field import create_custom_fields
from frappe.model.document import Document
from frappe.utils import call_hook_method, flt, get_url, get_url_to_form
from requests.exceptions import HTTPError

from payments.payment_gateways.doctype.sumup_settings.api.payouts import SumUpPayoutsAPI
from payments.payment_gateways.doctype.sumup_settings.api.transactions import SumUpTransactionsAPI
from payments.utils import create_payment_gateway

PAYMENT_STATUSES = {
	"success": ["SUCCESSFUL"],
	"failure": ["CANCELLED", "FAILED"],
	"pending": ["PENDING"]
}

class SumupSettings(Document):
	def on_update(self):
		create_payment_gateway(
			"Sumup", settings="Sumup Settings"
		)
		call_hook_method("payment_gateway_enabled", gateway="Sumup")

		if "erpnext" in frappe.get_installed_apps():
			create_custom_pos_fields()

	def get_secret_key(self):
		return self.get_password("secret_key", raise_exception=False)

	def on_payment_success(self, payment_request_doctype: str, payment_request: str):
		payment_request_doc = frappe.get_doc(payment_request_doctype, payment_request)
		payment = {}
		if transaction_code := payment_request_doc.get("transaction_reference") or (
			frappe.form_dict and frappe.form_dict.get("smp-tx-code")
		):
			payment = self.get_transaction(transaction_code)

		status = "Pending"
		reference_no = None
		if payment:
			if payment.get("status") in PAYMENT_STATUSES["success"]:
				status = "Paid"
			elif payment.get("status") in PAYMENT_STATUSES["failure"]:
				status = "Failed"

			reference_no = payment.get("id")

		try:
			payment_request_doc.run_method("on_payment_authorized", status=status, reference_no=reference_no)
			frappe.db.commit() # To fix: Can currently be called from a GET method
		except Exception:
			frappe.log_error("Sumup payment entry creation error")
			frappe.db.commit()

		frappe.local.flags.redirect_location = get_url_to_form(payment_request_doc.reference_doctype, payment_request_doc.reference_name)
		raise frappe.Redirect


	def get_transaction(self, transaction_code):
		api = self.get_transactions_api()

		try:
			return api.get(transaction_code=transaction_code)
		except HTTPError:
			self.log_error()
			return {}

	def get_transactions_api(self):
		return SumUpTransactionsAPI(self)

	def get_payout_api(self):
		return SumUpPayoutsAPI(self)


@frappe.whitelist()
def get_sumup_app_link(
	total,
	currency,
	title,
	reference_name,
	reference_doctype,
	mobile_no=None,
	email=None,
):
	ref_docs = ["Payment Request", "Sales Invoice"]
	if reference_doctype not in ref_docs:
		frappe.throw(_("The reference document should be one of the following: {0}").format(", ".join(ref_docs)))

	settings = frappe.get_single("Sumup Settings")
	transaction_id = reference_name
	if reference_doctype in ["Sales Invoice", "POS Invoice"]:
		payment_request = create_payment_request(
			reference_doctype=reference_doctype,
			reference_name=reference_name,
			grand_total=flt(total),
			currency=currency,
			payment_gateway=frappe.db.get_value("Payment Gateway", dict(gateway_settings=settings.name, disabled=0)),
		)
		transaction_id = payment_request

	callback_url = get_url("/sumup")

	args = {
		"affiliate-key": settings.affiliate_code,
		"app-id": settings.app_id,
		"total": total,
		"currency": currency,
		"title": title,
		"skip-screen-success": bool(settings.skip_success_screen),
		"foreign-tx-id": transaction_id,
		"callback": callback_url
	}

	if email:
		args["receipt-email"] = email

	if mobile_no:
		args["receipt-mobilephone"] = email

	frappe.db.set_value("Payment Request", transaction_id, "status", "Pending")

	return {
		"payment_app_url": f"sumupmerchant://pay/1.0?{urlencode(args)}",
		"name": transaction_id
	}


def create_payment_request(
	reference_doctype=None,
	reference_name=None,
	grand_total=None,
	currency=None,
	payment_gateway=None,
):
	existing_payment_request_amount = flt(
		get_existing_payment_request_amount(reference_doctype, reference_name)
	)

	if (grand_total - existing_payment_request_amount) <= 0:
		return frappe.db.get_value("Payment Request", dict(
			reference_doctype=reference_doctype,
			reference_name=reference_name,
			docstatus=1
		))

	payment_request = make_payment_request(
		dt=reference_doctype,
		dn=reference_name,
		grand_total=grand_total,
		submit_doc=True,
		return_doc=True,
		mute_email=1,
		currency=currency,
		payment_gateway=payment_gateway,
	)
	return payment_request.name



def create_custom_pos_fields():
	"""Create custom fields corresponding to POS Settings and POS Invoice."""
	# For translations
	__ = _("Request for Payment")
	__ = _("Pay with SumUp")

	pos_field = {
		"POS Invoice": [
			{
				"fieldname": "request_for_payment",
				"label": "Request for Payment",
				"fieldtype": "Button",
				"hidden": 1,
				"insert_after": "company",
			},
		]
	}
	if not frappe.get_meta("POS Invoice").has_field("request_for_payment"):
		create_custom_fields(pos_field)

	record_dict = [
		{
			"doctype": "POS Field",
			"fieldname": "request_for_payment",
			"label": "Pay with SumUp",
			"fieldtype": "Button",
			"parenttype": "POS Settings",
			"parent": "POS Settings",
			"parentfield": "invoice_fields",
		},
	]
	create_pos_settings(record_dict)


def create_pos_settings(record_dict):
	for record in record_dict:
		if frappe.db.exists("POS Field", {"fieldname": record.get("fieldname")}):
			continue
		frappe.get_doc(record).insert(ignore_permissions=True)
