from payments.payment_gateways.doctype.sumup_settings.api.base import SumUpAPI


class SumUpReaderAPI(SumUpAPI):
	def get_list(self, params=None):
		self.url = f"v0.1/merchants/{self.gateway.merchant_code}/readers"
		return super().get_list(params=params)

	def get(self, id):
		self.url = f"v0.1/merchants/{self.gateway.merchant_code}/readers/{id}"
		return super().get()

	def create(self, data):
		self.url = f"v0.1/merchants/{self.gateway.merchant_code}/readers"
		return super().post(data=data)
