from payments.payment_gateways.doctype.sumup_settings.api.base import SumUpAPI


class SumUpTransactionsAPI(SumUpAPI):
	def get(self, id=None, internal_id=None, transaction_code=None):
		self.url = f"v2.1/merchants/{self.gateway.merchant_code}/transactions"
		params = dict(
			id=id,
			internal_id=internal_id,
			transaction_code=transaction_code
		)
		return super().get(params=params)
