from payments.payment_gateways.doctype.sumup_settings.api.base import SumUpAPI


class SumUpAPIKeysAPI(SumUpAPI):
	def get_list(self, params=None):
		self.url = f"v0.1/merchants/{self.gateway.merchant_code}/api-keys"
		return super().get_list(params=params)

	def get(self, key_id):
		self.url = f"v0.1/merchants/{self.gateway.merchant_code}/api-keys/{key_id}"
		return super().get()
