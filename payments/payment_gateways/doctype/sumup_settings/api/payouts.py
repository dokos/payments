from payments.payment_gateways.doctype.sumup_settings.api.base import SumUpAPI


class SumUpPayoutsAPI(SumUpAPI):
	def get_list(self, start_date=None, end_date=None):
		self.url = "v0.1/me/financials/payouts"
		params = dict(
			start_date=start_date,
			end_date=end_date,
		)
		return super().get_list(params=params)
