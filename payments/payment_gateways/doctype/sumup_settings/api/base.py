from typing import TYPE_CHECKING

import frappe
from frappe.integrations.utils import make_get_request, make_patch_request, make_post_request
from frappe.utils import get_request_session
from requests.auth import HTTPBasicAuth

if TYPE_CHECKING:
	from payments.payment_gateways.doctype.sumup_settings.sumup_settings import SumupSettings


class SumUpAPI:
	def __init__(self, gateway):
		self.base = "https://api.sumup.com/"
		self.gateway: SumupSettings = gateway
		self.headers = {
			"Authorization": f"Bearer {gateway.get_secret_key()}",
			"Content-Type": "application/json",
		}

	def get(self, params=None):
		return make_get_request(url=f"{self.base}{self.url}", headers=self.headers, params=params or {})

	def post(self, data=None):
		return make_post_request(url=f"{self.base}{self.url}", headers=self.headers, json=data or {})

	def patch(self, data=None):
		return make_patch_request(url=f"{self.base}{self.url}", headers=self.headers, json=data or {})

	def delete(self):
		# We can't use make_delete_request because of a JSONDecodeError with the response
		try:
			s = get_request_session()
			response = frappe.flags.integration_request = s.request(
				"DELETE", f"{self.base}{self.url}", headers=self.headers
			)
			return response
		except Exception as exc:
			frappe.log_error()
			raise exc

	def get_list(self, params=None):
		return self.get(params)

	def create(self, data):
		return self.post(data)

	def update(self, data):
		return self.patch(data)
