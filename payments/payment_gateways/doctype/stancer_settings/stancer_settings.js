// Copyright (c) 2024, Frappe Technologies and contributors
// For license information, please see license.txt
/* eslint-disable no-unreachable */

frappe.ui.form.on("Stancer Settings", {
  refresh(frm) {
    if (!frm.is_new()) {
      return; // Webhooks are not yet available
      frm.add_custom_button(
        __("Create Stancer Webhooks"),
        () => {
          frappe.confirm(
            __("Configure webhooks on your Stancer Dashboard"),
            () => {
              frappe
                .call({
                  method:
                    "payments.payment_gateways.doctype.stancer_settings.stancer_settings.create_delete_webhooks",
                  args: {
                    settings: frm.doc.name,
                    action: "create",
                  },
                })
                .done((r) => {
                  if (r && r.message) {
                    frappe.show_alert({
                      message: __("Webhooks creation in progress"),
                      indicator: "green",
                    });
                    frm.reload_doc();
                  } else {
                    frappe.show_alert({
                      message: __(
                        "Webhooks creation failed. Please check the error logs"
                      ),
                      indicator: "red",
                    });
                  }
                });
            }
          );
        },
        __("Webhooks")
      );

      frm.add_custom_button(
        __("Delete Stancer Webhooks"),
        () => {
          frappe.confirm(
            __("Delete webhooks configured on your Stancer Dashboard"),
            () => {
              frappe
                .call({
                  method:
                    "payments.payment_gateways.doctype.stancer_settings.stancer_settings.create_delete_webhooks",
                  args: {
                    settings: frm.doc.name,
                    action: "delete",
                  },
                })
                .done((r) => {
                  if (r && r.message) {
                    frappe.show_alert({
                      message: __("Webhooks deletion in progress"),
                      indicator: "green",
                    });
                    frm.reload_doc();
                  } else {
                    frappe.show_alert({
                      message: __(
                        "Webhooks deletion failed. Please check the error logs"
                      ),
                      indicator: "red",
                    });
                  }
                });
            }
          );
        },
        __("Webhooks")
      );

      frm.add_custom_button(
        __("Show Stancer Webhooks"),
        () => {
          frappe
            .call({
              method:
                "payments.payment_gateways.doctype.stancer_settings.stancer_settings.get_webhooks",
              args: {
                settings: frm.doc.name,
              },
            })
            .done((r) => {
              if (r && r.message) {
                const message = r.message.webhook_endpoints
                  .map((ep) => {
                    return `<div><span class="indicator-pill ${
                      ep.enabled ? "green" : "red"
                    } mr-2">${
                      ep.enabled ? __("Enabled") : __("Disabled")
                    }</span>${ep.url}</div>`;
                  })
                  .join("");
                frappe.msgprint(message || __("No webhooks configured"));
              } else {
                frappe.show_alert({
                  message: __(
                    "Webhooks fetching failed. Please check the error logs"
                  ),
                  indicator: "red",
                });
              }
            });
        },
        __("Webhooks")
      );
    }
  },
});
