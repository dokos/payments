# Copyright (c) 2024, Frappe Technologies and Contributors
# For license information, please see license.txt

from unittest.mock import patch

import frappe
from erpnext.selling.doctype.sales_order.test_sales_order import make_sales_order
from frappe.tests.utils import FrappeTestCase
from frappe.utils.make_random import get_random

from payments.payment_gateways.doctype.stancer_settings.api import (
	StancerCustomersAPI,
	StancerPaymentIntentsAPI,
)

class TestStancerSettings(FrappeTestCase):
	@classmethod
	def setUpClass(cls) -> None:
		frappe.get_doc(
			{
				"doctype": "Stancer Settings",
				"gateway_name": "Stancer",
				"public_key": "ptest_xoSJDtq5YgZ2VPOe3ppcrnZS",
				"private_key": "stest_xoSJDtq5YgZ2VPOe3ppcrnZS",
				"card": 1,
				"sepa": 1,
			}
		).insert(ignore_if_duplicate=True)

	def test_create_payment_intent(self):
		so = make_sales_order(
			company="_Test Company",
			warehouse="Stores - _TC",
			customer="_Test Customer",
			selling_price_list="_Test Price List",
			item_code=get_random("Item", filters={"disabled": 0, "has_variants": 0}),
			rate=5000,
			qty=1,
			do_not_submit=False,
		)

		payment_request = make_payment_request(so.doctype, so.name)
		payment_data = get_payment_data(payment_request)

		settings = frappe.get_doc("Stancer Settings", "Stancer")
		with (
			patch.object(StancerCustomersAPI, "create", return_value={"id": "x115644654651dcsdcs"}),
			patch.object(StancerPaymentIntentsAPI, "create", return_value=successful_payment_intent_object),
		):
			url = settings.get_payment_url(**payment_data)
			self.assertEqual(url, "https://dokos.io?lang=fr")

			payment_request.reload()
			self.assertEqual(payment_request.transaction_reference, successful_payment_intent_object["id"])

	def test_payment_method_registration(self):
		so = make_sales_order(
			company="_Test Company",
			warehouse="Stores - _TC",
			customer="_Test Customer",
			selling_price_list="_Test Price List",
			item_code=get_random("Item", filters={"disabled": 0, "has_variants": 0}),
			rate=5000,
			qty=1,
			do_not_submit=False,
		)

		payment_request = make_payment_request(so.doctype, so.name)
		payment_data = get_payment_data(payment_request)

		settings = frappe.get_doc("Stancer Settings", "Stancer")

		with (
			patch.object(StancerCustomersAPI, "create", return_value={"id": mock_customer_id}),
			patch.object(StancerPaymentIntentsAPI, "create", return_value=successful_payment_intent_object),
			patch.object(
				StancerPaymentIntentsAPI, "get_payments", return_value=successful_payment_intent_object
			),
		):
			# 1. Generate payment URL to create payment intent
			settings.get_payment_url(**payment_data)
			frappe.db.commit()

			# 2. Landing after payment has been registered
			self.assertRaises(
				frappe.exceptions.Redirect,
				settings.on_payment_success,
				"Payment Request",
				payment_request.name,
			)

			# 2. Card should be registered after landing
			customer = frappe.get_doc("Customer", so.customer)
			self.assertEqual(customer.payment_gateways_references[0].customer_id, mock_customer_id)
			self.assertEqual(customer.payment_gateways_references[0].payment_method, "card")
			self.assertEqual(customer.payment_gateways_references[0].payment_method_id, mock_card_id)


def make_payment_request(reference_doctype=None, reference_name=None):
	ref = frappe.get_doc(reference_doctype, reference_name)
	payment_request = frappe.get_doc(
		{
			"doctype": "Payment Request",
			"grand_total": ref.grand_total,
			"reference_doctype": reference_doctype,
			"reference_name": reference_name,
			"payment_gateway": "Stancer-Stancer",
		}
	).insert(ignore_mandatory=True)

	return payment_request


def get_payment_data(payment_request):
	return {
		"amount": payment_request.grand_total,
		"title": "Tes Payment",
		"description": "Test Payment",
		"reference_doctype": "Payment Request",
		"reference_docname": payment_request.name,
		"payer_email": "hello@dokos.io",
		"payer_name": "Test User",
		"order_id": payment_request.name,
		"currency": "EUR",
		"payment_key": payment_request.payment_key,
	}


mock_customer_id = "cust_yWYfCSzsUUhr9KwMv7vuLZHX"
mock_card_id = "card_yWYfCSzsUUhr9KwMv7vuLZHX"

successful_payment_intent_object = {
	"id": "pi_yWYfCSzsUUhr9KwMv7vuLZHX",
	"customer": mock_customer_id,
	"payment": "paym_yWYfCSzsUUhr9KwMv7vuLZHX",
	"methods_allowed": ["card"],
	"order_id": "string",
	"card": "card_yWYfCSzsUUhr9KwMv7vuLZHX",
	"sepa": "sepa_yWYfCSzsUUhr9KwMv7vuLZHX",
	"amount": 10000,
	"currency": "str",
	"description": "string",
	"status": "require_payment_method",
	"metadata": None,
	"url": "https://dokos.io",
	"threeds": "required",
	"return_url": "https://dokos.io",
	"capture": True,
	"payments": [
		{
			"id": "paym_yWYfCSzsUUhr9KwMv7vuLZHX",
			"amount": 50,
			"auth": {},
			"capture": True,
			"created": "1512518400",
			"currency": "eur",
			"customer": {
				"id": mock_customer_id,
				"country": "FR",
				"created": "UniqueId123",
				"date_birth": "1950-05-04",
				"email": "test@test.com",
				"external_id": "UniqueId123",
				"legal_id": "AA732A82C93V20",
				"mobile": "+33100000000",
				"name": "Arthur Martin",
				"deleted": True,
			},
			"date_settlement": "2021-09-10",
			"date_bank": "2021-09-11",
			"description": "A simple description",
			"device": {},
			"fee": 2,
			"method": "card",
			"methods_allowed": [],
			"order_id": "NonUniqueId123",
			"unique_id": "UniqueId123",
			"response": "00",
			"response_author": "string",
			"return_url": "https://dokos.io",
			"card": {
				"customer": [
					mock_customer_id,
					"cust_SEqIZS1c23dfKculquz8idzR",
					"cust_Mu73X3xO3IEjjj8MlPsSGLbV",
					"cust_tY9xDPqoN4xxAuSkgElNacab",
					"cust_5O6cpeO4Ofy1QqY2NfdzheAD",
				],
				"brand": "visa",
				"country": "FR",
				"created": "1512518400",
				"exp_month": 12,
				"exp_year": 2029,
				"external_id": "UniqueId123",
				"funding": "credit",
				"id": mock_card_id,
				"last4": "4242",
				"name": "My card",
				"nature": "personal",
				"network": "visa",
				"zip_code": "75001",
				"deleted": True,
			},
			"sepa": {},
			"status": "refused",
		}
	],
}
