from payments.payment_gateways.doctype.stancer_settings.api.base import StancerAPI


class StancerDisputesAPI(StancerAPI):
	def __init__(self, gateway):
		super().__init__(gateway)
		self.url = "disputes"

	def get_one(self, id):
		self.url = f"disputes/{id}"
		return super().get()

