from payments.payment_gateways.doctype.stancer_settings.api.base import StancerAPI


class StancerPayoutsAPI(StancerAPI):
	def __init__(self, gateway):
		super().__init__(gateway)
		self.url = "payouts"

	def get_one(self, id):
		self.url = f"payouts/{id}"
		return super().get()
