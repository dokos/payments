from dataclasses import dataclass

import frappe

from payments.payment_gateways.doctype.stancer_settings.api.base import StancerAPI


class StancerCustomersAPI(StancerAPI):
	def __init__(self, gateway):
		super().__init__(gateway)
		self.url = "customers"

	def create(self, data):
		self.url = "customers"
		return super().create(data)

	def get_one(self, id):
		self.url = f"customers/{id}"
		return super().get()

	def delete(self, id):
		self.url = f"customers/{id}"
		return super().delete()


@dataclass
class StancerCustomer(frappe._dict):
	email: str | None
	name: str | None
	mobile: str | None = None
	date_birth: str | None = None
	legal_id: str | None = None
	country: str | None = None
	external_id: list | None = None
