from payments.payment_gateways.doctype.stancer_settings.api.base import StancerAPI


class StancerCardsAPI(StancerAPI):
	def __init__(self, gateway):
		super().__init__(gateway)
		self.url = "cards"

	def get_one(self, id):
		self.url = f"cards/{id}"
		return super().get()

	def delete(self, id):
		self.url = f"cards/{id}"
		return super().delete()
