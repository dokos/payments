from dataclasses import dataclass

import frappe

from payments.payment_gateways.doctype.stancer_settings.api.base import StancerAPI


class StancerWebhooksAPI(StancerAPI):
	def __init__(self, gateway):
		super().__init__(gateway)
		self.url = "webhooks"

	def enable(self, id):
		return self.update(id, dict(enabled=1))

	def disable(self, id):
		return self.update(id, dict(enabled=0))

	def delete(self, id):
		self.url = f"webhooks/{id}"
		return super().delete()


@dataclass
class StancerWebhook(frappe._dict):
	url: str | None
	enabled: bool | None = True
