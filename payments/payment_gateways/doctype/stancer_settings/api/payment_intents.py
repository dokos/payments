from dataclasses import dataclass

import frappe

from payments.payment_gateways.doctype.stancer_settings.api.base import StancerAPI


class StancerPaymentIntentsAPI(StancerAPI):
	def __init__(self, gateway):
		super().__init__(gateway)
		self.url = "payment_intents"

	def get_one(self, id):
		self.url = f"payment_intents/{id}"
		return super().get()

	def get_payments(self, id, params={}):
		self.url = f"payment_intents/{id}/payments"
		return self.get_list(params)

	def capture(self, id):
		self.url = f"payment_intents/{id}/capture"
		self.post()


@dataclass
class StancerPaymentIntent(frappe._dict):
	amount: int
	description: str | None = None
	currency: str | None = "eur"
	customer: str | None = None
	order_id: str | None = None
	methods_allowed: list | None = None
	card: str | None = None
	sepa: str | None = None
	metadata: str | None = None
	return_url: str | None = None
	threeds: str | None = "required"
	capture: bool | None = True
