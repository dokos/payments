from typing import TYPE_CHECKING

import frappe
from frappe.integrations.utils import make_get_request, make_patch_request, make_post_request
from frappe.utils import get_request_session
from requests.auth import HTTPBasicAuth

if TYPE_CHECKING:
	from payments.payment_gateways.doctype.stancer_settings.stancer_settings import StancerSettings


class StancerAPI:
	def __init__(self, gateway):
		self.base = "https://api.stancer.com/v2/"
		self.gateway: StancerSettings = gateway
		self.auth = HTTPBasicAuth(gateway.get_secret_key(), "")

	def get(self, params=None):
		if not params:
			params = {}
		return make_get_request(url=f"{self.base}{self.url}", auth=self.auth, params=params)

	def post(self, data=None):
		if not data:
			data = {}
		return make_post_request(url=f"{self.base}{self.url}", auth=self.auth, json=data)

	def patch(self, data=None):
		if not data:
			data = {}
		return make_patch_request(url=f"{self.base}{self.url}", auth=self.auth, json=data)

	def delete(self):
		# We can't use make_delete_request because of a JSONDecodeError with the response
		try:
			s = get_request_session()
			response = frappe.flags.integration_request = s.request(
				"DELETE", f"{self.base}{self.url}", auth=self.auth
			)
			return response
		except Exception as exc:
			frappe.log_error()
			raise exc

	def get_list(self, params=None):
		if not params:
			params = {}

		return self.get(params)

	def create(self, data):
		return self.post(data)

	def update(self, data):
		return self.patch(data)
