from dataclasses import dataclass

import frappe
from frappe.utils import get_request_session

from payments.payment_gateways.doctype.stancer_settings.api.base import StancerAPI


class StancerRefundsAPI(StancerAPI):
	def __init__(self, gateway):
		super().__init__(gateway)
		self.url = "refunds"

	def get_one(self, id):
		self.url = f"refunds/{id}"
		return super().get()

	def post(self, data=None):
		if not data:
			data = {}

		try:
			s = get_request_session()
			response = frappe.flags.integration_request = s.request(
				"POST", f"{self.base}{self.url}", auth=self.auth, json=data
			)
			if response.status_code == 409:
				return response.json()

			response.raise_for_status()

			return response.json()

		except Exception as exc:
			frappe.log_error()
			raise exc


@dataclass
class StancerRefunds(frappe._dict):
	amount: int
	payment: str
