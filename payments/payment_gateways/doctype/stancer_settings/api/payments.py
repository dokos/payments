from dataclasses import dataclass

import frappe

from payments.payment_gateways.doctype.stancer_settings.api.base import StancerAPI


class StancerPaymentsAPI(StancerAPI):
	def __init__(self, gateway):
		super().__init__(gateway)
		self.url = "payments"

	def get_one(self, id):
		self.url = f"payments/{id}"
		return super().get()

	def get_refunds(self, id, params={}):
		self.url = f"payments/{id}/refunds"
		return self.get_list(params)

	def get_disputes(self, id, params={}):
		self.url = f"payments/{id}/disputes"
		return self.get_list(params)


@dataclass
class StancerPayment(frappe._dict):
	amount: int
	description: str | None = None
	currency: str | None = "eur"
	customer: str | None = None
	order_id: str | None = None
	unique_id: str | None = None
	card: str | None = None
	sepa: str | None = None
	return_url: str | None = None
	capture: bool | None = True
	methods_allowed: list | None = None
