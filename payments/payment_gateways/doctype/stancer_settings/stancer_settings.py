# Copyright (c) 2024, Frappe Technologies and contributors
# For license information, please see license.txt

from urllib.parse import quote_plus

import frappe
from frappe import _
from frappe.contacts.doctype.address.address import get_default_address
from frappe.contacts.doctype.contact.contact import get_default_contact
from frappe.utils import call_hook_method, flt, get_url

from payments.payment_gateways.doctype.stancer_settings.api import (
	StancerCustomer,
	StancerCustomersAPI,
	StancerDisputesAPI,
	StancerPayment,
	StancerPaymentIntent,
	StancerPaymentIntentsAPI,
	StancerPaymentsAPI,
	StancerRefunds,
	StancerRefundsAPI,
	StancerWebhook,
	StancerWebhooksAPI,
)
from payments.utils import create_payment_gateway
from payments.utils.utils import PaymentGatewayController

WEBHOOK_ENDPOINT = "/api/method/payments.payment_gateways.doctype.stancer_settings.webhooks?account="

PAYMENT_STATUSES = {
	"pending": ["authorized", "available", "requested", "attempted"],
	"success": ["capture_sent", "to_capture", "captured", "success"],
	"failure": ["disputed", "expired", "failed", "refused", "declined", "unavailable"],
}


class StancerSettings(PaymentGatewayController):
	currency_wise_minimum_charge_amount = {  # noqa: RUF012
		"EUR": 0.50,
	}

	def validate(self):
		if not self.get_payment_methods():
			frappe.throw(_("Please select at least one payment method"))

	def get_secret_key(self):
		return self.get_password("private_key", raise_exception=False)

	def get_public_key(self):
		return self.public_key

	def validate_transaction_currency(self, currency):
		if currency not in ["EUR", "USD", "GBP", "CAD", "AUD", "CHF", "DKK", "NOK", "PLN", "SEK"]:
			frappe.throw(
				_(
					"Please select another payment method. Stancer does not support transactions in currency '{0}'"
				).format(currency)
			)

	def on_update(self):
		create_payment_gateway(
			"Stancer-" + self.gateway_name, settings="Stancer Settings", controller=self.gateway_name
		)
		call_hook_method("payment_gateway_enabled", gateway="Stancer-" + self.gateway_name)

	def get_customers_api(self):
		return StancerCustomersAPI(self)

	def get_payment_intents_api(self):
		return StancerPaymentIntentsAPI(self)

	def get_payments_api(self):
		return StancerPaymentsAPI(self)

	def get_disputes_api(self):
		return StancerDisputesAPI(self)

	def get_refunds_api(self):
		return StancerRefundsAPI(self)

	def get_payment_disputes(self, payment_id):
		payments_api = self.get_payments_api()
		return payments_api.get_disputes(payment_id)

	def get_dispute(self, dispute_id):
		dispute_api = self.get_disputes_api()
		return dispute_api.get_one(dispute_id)

	def create_refund(self, payment_id, amount):
		if flt(amount) <= 0:
			frappe.throw(_("The refunded amount must be positive"))

		refund = StancerRefunds(
			payment=payment_id,
			amount=round(flt(amount) * 100)
		)

		refunds_api = self.get_refunds_api()
		return refunds_api.create(data=dict(**refund))

	def get_payment_url(self, **kwargs):
		data = kwargs

		payment_intent = self.create_payment_intent(**data)

		if payment_intent and payment_intent.get("url"):
			return f"{payment_intent.get('url')}?lang=fr"
		else:
			return get_url("payment-failed")

	def get_payment_methods(self):
		payment_methods = []
		if self.card:
			payment_methods.append("card")
		if self.sepa:
			payment_methods.append("sepa")
		return payment_methods

	def get_customer_id(self, customer):
		if payment_gateway := frappe.db.get_value(
			"Payment Gateway",
			dict(gateway_settings="Stancer Settings", gateway_controller=self.name, disabled=0),
		):
			return frappe.db.get_value(
				"Payment Gateways References",
				dict(
					parenttype="Customer",
					parent=customer,
					payment_gateway=payment_gateway,
				),
				"customer_id",
			)

	def get_customer_references(self, customer, payment_gateway):
		customer = frappe.get_cached_doc("Customer", customer)
		if references := customer.get(
			"payment_gateways_references", dict(payment_gateway=payment_gateway), limit=1
		):
			return references[0]

		return {}

	def get_or_create_customer(self, payment_request_doctype, payment_request):
		payment_request_doc = frappe.get_doc(payment_request_doctype, payment_request)
		if not payment_request_doc.customer:
			return

		customer_doc = frappe.get_cached_doc("Customer", payment_request_doc.customer)
		customer_api = self.get_customers_api()

		if stancer_id := customer_doc.get(
			"payment_gateways_references", dict(payment_gateway=payment_request_doc.payment_gateway)
		):
			try:
				res = customer_api.get_one(stancer_id[0].customer_id)
				return res.get("id")
			except Exception:
				frappe.delete_doc(
					"Payment Gateways References", stancer_id[0].name, force=True, ignore_permissions=True
				)

		primary_contact = self.get_primary_contact(customer_doc) or frappe._dict()
		primary_address = self.get_primary_address(customer_doc) or frappe._dict()
		country_code = None
		if primary_address and primary_address.country:
			country_code = frappe.db.get_value("Country", primary_address.country, "code")

		customer_obj = StancerCustomer(
			name=customer_doc.customer_name,
			email=payment_request_doc.get("email_to")
			or payment_request_doc.get("email")
			or primary_contact.get("email_id"),
			mobile=parse_phone_number(primary_contact.mobile_no, country_code),
			legal_id=customer_doc.tax_id,
			country=country_code,
			external_id=customer_doc.name,
		)

		stancer_customer = customer_api.create(data=dict(**customer_obj))

		customer_doc.append(
			"payment_gateways_references",
			{
				"payment_gateway": payment_request_doc.payment_gateway,
				"customer_id": stancer_customer.get("id"),
			},
		)
		customer_doc.flags.ignore_permissions = True
		customer_doc.save()

		return stancer_customer.get("id")

	@staticmethod
	def get_primary_contact(customer):
		if customer.customer_primary_contact:
			contact = frappe.db.get_value("Contact", customer.customer_primary_contact, "name")
		else:
			contact = get_default_contact(customer.doctype, customer.name)

		if contact:
			return frappe.get_doc("Contact", contact)

	@staticmethod
	def get_primary_address(customer):
		if customer.customer_primary_address:
			address = frappe.db.get_value("Address", customer.customer_primary_address, "name")
		else:
			address = get_default_address(customer.doctype, customer.name)

		if address:
			return frappe.get_doc("Address", address)

	def create_payment_intent(self, **kwargs):
		data = kwargs

		customer_id = data.get("customer_id") or self.get_or_create_customer(
			data.get("reference_doctype"), data.get("reference_docname")
		)

		default_return_url = f"payment-return?reference_doctype={quote_plus(data.get('reference_doctype'))}&reference_docname={quote_plus(data.get('reference_docname'))}"
		payment_intent_api = self.get_payment_intents_api()
		payment_intent = StancerPaymentIntent(
			description=data.get("description") or data.get("title"),
			amount=round(flt(data.get("amount")) * 100),
			currency=data.get("currency").lower(),
			order_id=data.get("order_id"),
			return_url=get_url(
				f"payment-return?payment_key={data.get('payment_key')}"
				if data.get("payment_key")
				else default_return_url
			),
			methods_allowed=self.get_payment_methods(),
			customer=customer_id,
			card=data.get("card"),
			sepa=data.get("sepa"),
		)
		stancer_payment_intent = payment_intent_api.create(data=dict(**payment_intent))

		if frappe.db.has_column(data.get("reference_doctype"), "transaction_reference"):
			frappe.db.set_value(data["reference_doctype"], data["reference_docname"], "transaction_reference", stancer_payment_intent.get("id"))

		# For compatibility with web forms
		if frappe.db.has_column(data.get("reference_doctype"), "reference_no"):
			frappe.db.set_value(data["reference_doctype"], data["reference_docname"], "reference_no", stancer_payment_intent.get("id"))

		self.payment_session = self.get_or_create_payment_session(
			reference_doctype=data.get("reference_doctype"),
			reference_docname=data.get("reference_docname"),
			reference=stancer_payment_intent.get("id")
		)
		self.payment_session.db_set("data", str(stancer_payment_intent))

		return stancer_payment_intent

	def create_payment(self, **kwargs):
		data = kwargs

		customer_id = data.get("customer_id") or self.get_or_create_customer(data.get("order_id"))

		default_return_url = f"payment-return?reference_doctype={quote_plus(data.get('reference_doctype'))}&reference_docname={quote_plus(data.get('reference_docname'))}"
		payment_api = self.get_payments_api()
		payment = StancerPayment(
			description=data.get("description") or data.get("title"),
			amount=round(flt(data.get("amount")) * 100),
			currency=data.get("currency").lower(),
			order_id=data.get("order_id"),
			return_url=get_url(
				f"payment-return?payment_key={data.get('payment_key')}"
				if data.get("payment_key")
				else default_return_url
			),
			methods_allowed=self.get_payment_methods(),
			customer=customer_id,
			card=data.get("card"),
			sepa=data.get("sepa"),
			unique_id=data.get("unique_id"),
		)

		stancer_payment = payment_api.create(data=dict(**payment))

		return stancer_payment

	def on_payment_success(self, payment_request_doctype: str, payment_request: str):
		payment_request_doc = frappe.get_doc(payment_request_doctype, payment_request)
		payment = {}
		if payment_request_doc.get("transaction_reference"):
			payment = self.get_processed_payment(payment_request_doc.transaction_reference)
		elif payment_request_doc.get("reference_no"):
			payment = self.get_processed_payment(payment_request_doc.reference_no)

		if not payment:
			payments_api = self.get_payments_api()
			if payments := payments_api.get_list(dict(order_id=payment_request)):
				if payments.get("payments"):
					payment = next(iter(payments.get("payments")))

		status = "Failed"
		reference_no = None
		if payment:
			status = "Pending"
			if payment.get("status") in PAYMENT_STATUSES["success"]:
				status = "Paid"
			elif payment.get("status") in PAYMENT_STATUSES["failure"]:
				status = "Failed"

			reference_no = payment.get("id")
			self.register_customer(payment_request_doc, payment)

		if not reference_no and payment_request_doc.get("transaction_reference"):
			reference_no = payment_request_doc.transaction_reference

		elif not reference_no and payment_request_doc.get("reference_no"):
			reference_no = payment_request_doc.reference_no

		try:
			self.update_payment_session(payment_request_doc, status, reference_no, payment)
			payment_request_doc.run_method("on_payment_authorized", status=status, reference_no=reference_no)
			frappe.db.commit() # To fix: Can currently be called from a GET method

		except Exception:
			frappe.log_error("Stancer payment entry creation error")
			frappe.db.commit()

		frappe.local.flags.redirect_location = "payment-success" if status == "Paid" else "payment-failed"
		raise frappe.Redirect

	def get_processed_payment(self, ref: str):
		if not isinstance(ref, str):
			return {}

		if ref.startswith("pi_"):
			payment_intents_api = self.get_payment_intents_api()
			if payments := payment_intents_api.get_payments(ref):
				if payments.get("payments"):
					*_, last = iter(payments.get("payments"))
					return last
		else:
			payments_api = self.get_payments_api()
			if payment := payments_api.get_one(ref):
				return payment

		return {}

	def register_customer(self, payment_request, payment):
		from erpnext.accounts.doctype.payment_request.payment_request import get_mode_of_payment_for_company

		if payment.get("customer") and payment.get("customer", {}).get("id"):
			customer = payment_request.customer
			payment_gateway = frappe.db.get_value(
				"Payment Gateway",
				dict(gateway_settings="Stancer Settings", gateway_controller=self.name, disabled=0),
			)

			payment_methods = ["sepa", "card"]
			for pm_name in payment_methods:
				if pm_info := payment.get(pm_name):
					payment_method = pm_name
					payment_method_id = pm_info.get("id")
					break

			for row in frappe.get_all("Payment Gateways References", filters={
				"parenttype": "Customer",
				"parent": customer,
				"payment_gateway": payment_gateway,
			}, fields=["customer_id", "payment_method", "payment_method_id", "name"]):
				if not (
					row.customer_id == payment.get("customer", {}).get("id")
					and row.payment_method == payment_method
					and row.payment_method_id == payment_method_id
				):
					frappe.db.set_value("Payment Gateways References", row.name, "customer_id", payment.get("customer", {}).get("id"))
					frappe.db.set_value("Payment Gateways References", row.name, "payment_method", payment_method)
					frappe.db.set_value("Payment Gateways References", row.name, "payment_method_id", payment_method_id)
			else:
				new_reference = frappe.new_doc("Payment Gateways References")
				new_reference.parenttype = "Customer"
				new_reference.parent = customer
				new_reference.update({
					"payment_gateway": payment_gateway,
					"customer_id": payment.get("customer", {}).get("id"),
					"payment_method": payment_method,
					"payment_method_id": payment_method_id,
				})
				new_reference.insert(ignore_permissions=True)

			if not frappe.db.get_value("Customer", customer, "mode_of_payment"):
				mode_of_payment = get_mode_of_payment_for_company(payment_request.company, payment_gateway)
				frappe.db.set_value("Customer", customer, "mode_of_payment", mode_of_payment)


	def immediate_payment_processing(self, reference, customer, amount, currency, description, metadata):
		try:
			payment_request = frappe.get_doc("Payment Request", reference)
			customer_reference = self.get_customer_references(customer, payment_request.payment_gateway)
			customer_id = customer_reference.get("customer_id")
			payment = self.create_payment(
				**{
					"customer_id": customer_id,
					"description": description,
					"amount": amount,
					"currency": currency,
					"order_id": payment_request.name,
					"card": customer_reference.payment_method_id
					if customer_reference.payment_method == "card"
					else None,
					"sepa": customer_reference.payment_method_id
					if customer_reference.payment_method == "sepa"
					else None,
					"unique_id": payment_request.name,
					"reference_doctype": payment_request.doctype,
					"reference_docname": payment_request.name,
				}
			)

			return payment.get("id")
		except Exception:
			frappe.log_error(
				_("Stancer direct processing failed for {0}").format(reference),
				message=frappe.get_traceback(),
				reference_doctype=isinstance(metadata, dict) and metadata.get("reference_doctype"),
			)

	def get_transaction_fees(self, payment):
		payments_api = self.get_payments_api()
		stancer_payment = payments_api.get_one(payment)
		fee_amount = flt(stancer_payment.get("fee")) / 100.0
		vat_amount = fee_amount * 0.2 # TODO: Hardcoded VAT on fees until we can change it after receiving the payout
		return frappe._dict(
			base_amount=(flt(stancer_payment.get("amount")) / 100.0),
			fee_amount=fee_amount,
			tax_amount=vat_amount,
			exchange_rate=1,
		)

@frappe.whitelist()
def create_delete_webhooks(settings, action="create"):
	stancer_settings = frappe.get_doc("Stancer Settings", settings)
	url = f"{frappe.utils.get_url(WEBHOOK_ENDPOINT)}{stancer_settings.name}"

	if action == "create":
		return create_webhooks(stancer_settings, url)
	elif action == "delete":
		return delete_webhooks(stancer_settings, url)


def create_webhooks(stancer_settings, url):
	try:
		webhooks_api = StancerWebhooksAPI(stancer_settings)
		webhook = StancerWebhook(url=url)
		result = webhooks_api.create(data=dict(**webhook))
		if result:
			frappe.db.set_value(
				"Stancer Settings", stancer_settings.name, "webhook_secret_key", result.get("secret")
			)
		return result
	except Exception:
		frappe.log_error(_("Stancer webhook creation error"))


def delete_webhooks(stancer_settings, url):
	webhooks_api = StancerWebhooksAPI(stancer_settings)
	webhooks_list = webhooks_api.get_list()

	for webhook in webhooks_list.get("webhook_endpoints", []):
		if webhook.get("url") == url:
			try:
				webhooks_api.delete(webhook.get("id"))
				frappe.db.set_value("Stancer Settings", stancer_settings.name, "webhook_secret_key", "")
			except Exception:
				frappe.log_error(_("Stancer webhook deletion error"))

	return webhooks_list


@frappe.whitelist()
def get_webhooks(settings):
	stancer_settings = frappe.get_doc("Stancer Settings", settings)
	webhooks_api = StancerWebhooksAPI(stancer_settings)
	return webhooks_api.get_list()


def handle_webhooks(**kwargs):
	integration_request = frappe.get_doc(kwargs.get("doctype"), kwargs.get("docname"))
	integration_request.handle_failure({"message": _("This type of event is not handled")}, "Not Handled")


def parse_phone_number(number: str, country_code: str):
	import phonenumbers

	try:
		if country_code:
			phonenumbers.parse(number, country_code.upper())
		else:
			phonenumbers.parse(number, None)
	except Exception:
		return None
