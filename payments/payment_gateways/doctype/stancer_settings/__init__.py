# Copyright (c) 2024, Dokos SAS and contributors
# For license information, please see license.txt

import hashlib
import hmac
import json
from datetime import datetime, timedelta, timezone
from urllib.parse import parse_qs

import frappe
from frappe import _


@frappe.whitelist(allow_guest=True)
def webhooks():
	r = frappe.request
	payload = json.loads(r.get_data()) or []
	if not payload:
		frappe.response.message = "Missing payload"
		frappe.response.http_status_code = 400

	account, webhook_secret = get_account_and_secret(r)

	event = None
	try:
		verify(
			secret=webhook_secret, payload=payload, signature=frappe.get_request_header("Stancer-Signature")
		)
		event = payload
	except Exception:
		frappe.response.message = "Webhook event construction failed"
		frappe.response.http_status_code = 400

	# Handle the event
	doc = create_new_integration_log(event, account)

	frappe.enqueue(
		method="payments.payment_gateways.doctype.stancer_settings.stancer_settings.handle_webhooks",
		queue="long",
		timeout=600,
		is_async=True,
		**{"doctype": "Integration Request", "docname": doc.name},
	)

	frappe.response.message = "OK"
	frappe.response.http_status_code = 200


def get_account_and_secret(request):
	account = None
	if request.query_string:
		parsed_qs = parse_qs(frappe.safe_decode(request.query_string))
		account = parsed_qs.get("account")

		if isinstance(account, list):
			account = account[0]

	if account:
		return account, frappe.get_doc("Stancer Settings", account).get_password(
			fieldname="webhook_secret_key", raise_exception=False
		)
	else:
		stancer_accounts = frappe.get_all("Stancer Settings")
		if len(stancer_accounts) > 1:
			frappe.log_error(
				message=_("Please define your Stancer account in the webhook URL's query string"),
				title=_("Stancer webhook error"),
			)
		else:
			return stancer_accounts[0].get("name"), frappe.get_doc(
				"Stancer Settings", stancer_accounts[0].get("name")
			).get_password(fieldname="webhook_secret_key", raise_exception=False)


def compute(secret: str, payload: str, date: datetime) -> list[int | str]:
	secret = bytes.fromhex(secret)
	timestamp = int(date.timestamp())
	signed_payload = (str(timestamp) + ".").encode() + payload
	return hmac.digest(secret, signed_payload, hashlib.sha256).hex()


def parse(signature: str, version="v1") -> list[int | str]:
	tmp = signature.split(",")
	parts = {}
	for t in tmp:
		t = t.split("=")
		parts[t[0]] = t[1]
	timestamp = int(parts["t"])
	return timestamp, parts[version]


def verify(
	secret: str,
	payload: str,
	signature: str,
	now: datetime | None = None,
	delta=timedelta(minutes=1),
) -> None:
	now = now or datetime.now(tz=timezone.utc)
	timestamp, expected = parse(signature)
	date = datetime.fromtimestamp(timestamp, tz=timezone.utc)
	if now - date > delta:
		raise Exception("Expired signature")
	_, computed = compute(secret, payload, date)
	ok = hmac.compare_digest(expected, computed)
	if not ok:
		raise Exception("Invalid signature")


def create_new_integration_log(event, account):
	integration_request = frappe.get_doc(
		{
			"doctype": "Integration Request",
			"request_description": "Webhook",
			"integration_request_service": "Stancer",
			"service_document": event.type.split(".")[0],
			"service_status": event.type.split(".")[-1],
			"service_id": event.data.object.get("id"),
			"data": json.dumps(event, indent=4),
			"payment_gateway_controller": account,
		}
	)

	integration_request.flags._name = event.id

	integration_request.insert(ignore_permissions=True, ignore_if_duplicate=True)
	frappe.db.commit()

	return integration_request
