import frappe
from itertools import groupby

from erpnext.accounts.doctype.sales_invoice.sales_invoice import make_sales_return

def execute():
	if not frappe.get_all("Stripe Settings"):
		return

	payment_entries = frappe.get_all("Payment Entry", filters={"payment_request": ("is", "set"), "docstatus": 1, "posting_date": (">", "2024-11-24")}, fields=["payment_request", "name"], order_by="payment_request")

	for _, group in groupby(payment_entries, lambda k:k["payment_request"]):
		data = list(group)

		if len(data) > 1:
			payment_to_cancel = data[0]
			pe = frappe.get_doc("Payment Entry", payment_to_cancel.get("name"))
			pe.cancel()

			for reference in pe.references:
				if reference.reference_doctype == "Sales Invoice":
					doc = make_sales_return(reference.reference_name)
					doc.insert()
					doc.submit()
					doc.add_comment("Comment", text="Duplicate entry from Stripe auto-corrected.")

					pr = frappe.get_doc("Payment Reconciliation")
					pr.company = doc.company
					pr.party_type = "Customer"
					pr.party = doc.customer
					pr.receivable_payable_account = doc.debit_to
					pr.default_advance_account = ""
					pr.get_unreconciled_entries()

					invoices = [x.as_dict() for x in pr.invoices if x.get("invoice_type") == "Sales Invoice" and x.get("invoice_number") == reference.reference_name]
					payments = [x.as_dict() for x in pr.payments if x.get("reference_type") == doc.doctype and x.get("reference_name") == doc.name]

					if invoices and payments:
						pr.allocate_entries(frappe._dict({"invoices": invoices, "payments": payments}))
						pr.reconcile()