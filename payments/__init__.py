import frappe

__version__ = "2.0.0"


def check_app_permission():
	"""Check if user has permission to access the app (for showing the app on app screen)"""
	if "erpnext" not in frappe.get_installed_apps():
		return True

	return False
