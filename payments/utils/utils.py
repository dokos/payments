from contextlib import contextmanager
from typing import TYPE_CHECKING
from urllib.parse import urlencode

import click
import frappe
from frappe import _
from frappe.custom.doctype.custom_field.custom_field import create_custom_fields
from frappe.model.document import Document
from frappe.utils import add_days, nowdate

if TYPE_CHECKING:
	from erpnext.selling.doctype.payment_gateways_references.payment_gateways_references import (
		PaymentGatewaysReferences,
	)


class PaymentGatewayController(Document):
	def finalize_request(self, reference_no=None):
		redirect_to = self.data.get("redirect_to")
		redirect_message = self.data.get("redirect_message")

		if self.flags.status_changed_to in ["Completed", "Autorized", "Pending"] and self.reference_document:
			custom_redirect_to = None
			try:
				custom_redirect_to = self.reference_document.run_method(
					"on_payment_authorized", self.flags.status_changed_to, reference_no
				)
			except Exception:
				frappe.log_error(frappe.get_traceback(), _("Payment custom redirect error"))

			if custom_redirect_to and custom_redirect_to != "no-redirection":
				redirect_to = custom_redirect_to

			redirect_url = self.redirect_url if self.get("redirect_url") else "/payment-success"

		else:
			redirect_url = "/payment-failed"

		if redirect_to and redirect_to != "no-redirection":
			redirect_url += "?" + urlencode({"redirect_to": redirect_to})
		if redirect_message:
			redirect_url += "&" + urlencode({"redirect_message": redirect_message})

		return {"redirect_to": redirect_url, "status": self.integration_request.status}

	def change_integration_request_status(self, status, error_type, error):
		if hasattr(self, "integration_request"):
			self.flags.status_changed_to = status
			self.integration_request.db_set("status", status, update_modified=True)
			self.integration_request.db_set(error_type, error, update_modified=True)

		if hasattr(self, "update_reference_document_status"):
			self.update_reference_document_status(status)

	def get_or_create_payment_session(self, reference_doctype, reference_docname, reference=None):
		query_filters = {
			"status": "Pending",
			"reference_doctype": reference_doctype,
			"reference_name": reference_docname
		}

		if reference:
			query_filters["payment_gateway_reference"] = reference

		if ps := frappe.db.get_value("Payment Session", reference):
			return frappe.get_doc("Payment Session", ps)

		return self.initiate_payment_session(reference_doctype=reference_doctype, reference_docname=reference_docname, reference=reference)


	def initiate_payment_session(self, reference_doctype, reference_docname, reference=None):
		return self.create_payment_session(reference_doctype, reference_docname, reference=reference)


	def create_payment_session(self, reference_doctype, reference_name, reference=None):
		reference_document = frappe.get_doc(reference_doctype, reference_name)

		payment_session = frappe.new_doc("Payment Session")
		payment_session.payment_gateway = reference_document.payment_gateway
		payment_session.reference_doctype = reference_doctype
		payment_session.reference_name = reference_name
		payment_session.payment_gateway_reference = reference
		payment_session.insert(ignore_permissions=True)

		return payment_session

	def update_payment_session(self, payment_request_doc, status, reference=None, payment=None):
		self.integration_request = self.get_or_create_payment_session(
			reference_doctype=payment_request_doc.doctype,
			reference_docname=payment_request_doc.name,
			reference=reference
		)

		if payment:
				self.integration_request.db_set("data", str(payment))

		if status == "Pending":
			return

		elif status == "Failed":
			self.integration_request.db_set("status", "Failed")

		elif status == "Paid":
			self.integration_request.db_set("status", "Success")


def validate_integration_request(docname: str | None):
	if frappe.db.get_value("Integration Request", docname, "status") == "Cancelled":
		frappe.throw(_("Expired Token"))


def get_payment_gateway_controller(payment_gateway):
	"""Return payment gateway controller"""
	gateway = frappe.get_doc("Payment Gateway", payment_gateway)
	if gateway.gateway_controller is None:
		try:
			return frappe.get_doc(f"{payment_gateway} Settings")
		except Exception:
			frappe.throw(_("{0} Settings not found").format(payment_gateway))
	else:
		try:
			return frappe.get_doc(gateway.gateway_settings, gateway.gateway_controller)
		except Exception:
			frappe.throw(_("{0} Settings not found").format(payment_gateway))


def get_gateway_controller(doctype, docname):
	payment_gateway = frappe.db.get_value(doctype, docname, "payment_gateway")
	gateway_controller = frappe.db.get_value("Payment Gateway", payment_gateway, "gateway_controller")
	return gateway_controller


@frappe.whitelist(allow_guest=True, xss_safe=True)
def get_checkout_url(**kwargs):
	try:
		if kwargs.get("payment_gateway"):
			doc = frappe.get_doc("{} Settings".format(kwargs.get("payment_gateway")))
			return doc.get_payment_url(**kwargs)
		else:
			raise Exception
	except Exception:
		frappe.respond_as_web_page(
			_("Something went wrong"),
			_(
				"Looks like something is wrong with this site's payment gateway configuration. No payment has been made."
			),
			indicator_color="red",
			http_status_code=frappe.ValidationError.http_status_code,
		)


def create_payment_gateway(gateway, settings=None, controller=None):
	# NOTE: we don't translate Payment Gateway name because it is an internal doctype
	if not frappe.db.exists("Payment Gateway", gateway):
		payment_gateway = frappe.get_doc(
			{
				"doctype": "Payment Gateway",
				"gateway": gateway,
				"gateway_settings": settings,
				"gateway_controller": controller,
			}
		)
		payment_gateway.insert(ignore_permissions=True)


def can_make_immediate_payment(payment_request, controller):
	from payments.payment_gateways.doctype.stripe_settings.api.customer import StripeCustomer

	customer = payment_request.get_customer()
	if not customer:
		return

	customer_doc = frappe.get_cached_doc("Customer", customer)
	payment_gateway_references = customer_doc.get(
		"payment_gateways_references", dict(payment_gateway=payment_request.payment_gateway), limit=1
	)
	if payment_request.payment_gateway and payment_gateway_references:
		reference: "PaymentGatewaysReferences" = payment_gateway_references[0]

		if controller.doctype == "Stripe Settings":
			if reference.customer_id:
				stripe_customer = StripeCustomer(controller).get(reference.customer_id)
				has_default_source = bool(stripe_customer.get("default_source"))
				has_default_payment_method = bool(
					stripe_customer.get("invoice_settings", {}).get("default_payment_method")
				)
				return has_default_source or has_default_payment_method

		elif controller.doctype == "GoCardless Settings":
			return bool(controller.check_mandate_validity(payment_request.get_customer()).get("mandate"))

		elif controller.doctype == "Stancer Settings":
			if reference.customer_id and reference.payment_method_id:
				return True

		else:
			return bool(reference.payment_method_id)


def get_customer_from_integration_id(payment_gateway, customer_id):
	return frappe.db.get_value(
		"Payment Gateways References",
		dict(parenttype="Customer", payment_gateway=payment_gateway, customer_id=customer_id),
		"parent",
	)


@frappe.whitelist()
def update_payment_request_status(payment_request):
	payment_request_doc = frappe.get_doc("Payment Request", payment_request)
	gateway_controller = get_payment_gateway_controller(payment_request_doc.payment_gateway)
	try:
		gateway_controller.run_method(
			"on_payment_success", payment_request_doctype="Payment Request", payment_request=payment_request
		)
	except frappe.exceptions.Redirect:
		pass


def update_payment_requests_statuses():
	gateways_with_on_payment_success_method = ["Stancer Settings", "HelloAsso Settings"]
	payment_gateways = frappe.get_all(
		"Payment Gateway",
		filters={"disabled": 0, "gateway_settings": ("in", gateways_with_on_payment_success_method)},
		pluck="name",
	)
	pending_statuses = ["Requested", "Pending", "Failed"]
	for pr in frappe.get_all(
		"Payment Request",
		filters={
			"status": ("in", pending_statuses),
			"payment_gateway": ("in", payment_gateways),
			"creation": (">=", add_days(nowdate(), -7)),
			"transaction_reference": ("is", "set")
		},
	):
		update_payment_request_status(pr.name)


def after_migrate():
	make_custom_fields()


def make_custom_fields():
	click.secho("* Updating Payment Custom Fields in Web Form")
	create_custom_fields(get_custom_fields())
	frappe.clear_cache(doctype="Web Form")


def get_custom_fields():
	# For translations
	__ = _("Payments")
	__ = _("Accept Payment")
	__ = _("Payment Gateway")
	__ = _("Payment Type")
	__ = _("Amount Based On Field")
	__ = _("Amount Field")
	__ = _("Amount")
	__ = _("Currency Based On Field")
	__ = _("Currency Field")
	__ = _("Currency")
	__ = _("Button Label")
	__ = _("Button Help")

	return {
		"Web Form": [
			{
				"fieldname": "payments_tab",
				"fieldtype": "Tab Break",
				"label": "Payments",
				"insert_after": "custom_css",
			},
			{
				"default": "0",
				"fieldname": "accept_payment",
				"fieldtype": "Check",
				"label": "Accept Payment",
				"insert_after": "payments_tab",
			},
			# Payment options section
			{
				"depends_on": "accept_payment",
				"fieldname": "payments_sb01",
				"fieldtype": "Section Break",
				"insert_after": "accept_payment",
			},
			{
				"fieldname": "payment_gateway",
				"fieldtype": "Link",
				"label": "Payment Gateway",
				"options": "Payment Gateway",
				"insert_after": "payments_sb01",
			},
			{"fieldname": "payments_cb01", "fieldtype": "Column Break", "insert_after": "payment_gateway"},
			{
				"default": "Immediate payment",
				"fieldname": "payment_type",
				"fieldtype": "Select",
				"label": "Payment Type",
				"options": "Immediate payment\nAutomatic payments\nInitial payment followed by automatic payments",
				"insert_after": "payments_cb01",
				"translatable": "0",
			},
			# Amount and currency section
			{
				"depends_on": "eval:doc.accept_payment && (typeof doc.payment_type !== 'string' || doc.payment_type.match(/immediate|initial/i))",
				"fieldname": "payments_amount_section",
				"fieldtype": "Section Break",
				"insert_after": "payment_type",
			},
			{
				"default": "0",
				"fieldname": "amount_based_on_field",
				"fieldtype": "Check",
				"label": "Amount Based On Field",
				"insert_after": "payments_amount_section",
			},
			{
				"depends_on": "eval:doc.amount_based_on_field",
				"fieldname": "amount_field",
				"fieldtype": "Select",
				"label": "Amount Field",
				"insert_after": "amount_based_on_field",
				"translatable": "0",
			},
			{
				"depends_on": "eval:!doc.amount_based_on_field",
				"fieldname": "amount",
				"fieldtype": "Currency",
				"label": "Amount",
				"insert_after": "amount_field",
				"translatable": "0",
			},
			{"fieldname": "payments_cb02", "fieldtype": "Column Break", "insert_after": "amount"},
			{
				"default": "0",
				"fieldname": "currency_based_on_field",
				"fieldtype": "Check",
				"label": "Currency Based On Field",
				"insert_after": "payments_cb02",
			},
			{
				"depends_on": "eval:doc.currency_based_on_field",
				"fieldname": "currency_field",
				"fieldtype": "Select",
				"label": "Currency Field",
				"insert_after": "currency_based_on_field",
				"translatable": "0",
			},
			{
				"depends_on": "eval:!doc.currency_based_on_field",
				"fieldname": "currency",
				"fieldtype": "Link",
				"label": "Currency",
				"options": "Currency",
				"insert_after": "currency_field",
				"translatable": "0",
			},
			# Misc section
			{
				"fieldname": "payments_misc_section",
				"fieldtype": "Section Break",
				"insert_after": "currency",
			},
			{
				"default": "Pay now",
				"depends_on": "accept_payment",
				"fieldname": "payment_button_label",
				"fieldtype": "Data",
				"label": "Button Label",
				"insert_after": "payment_gateway",
				"translatable": "1",
			},
			{
				"depends_on": "accept_payment",
				"fieldname": "payment_button_help",
				"fieldtype": "Text",
				"label": "Button Help",
				"insert_after": "payment_button_label",
			},
		],
	}


def delete_custom_fields():
	if frappe.get_meta("Web Form").has_field("payments_tab"):
		click.secho("* Uninstalling Payment Custom Fields from Web Form")

		fieldnames = (
			"payments_tab",
			"accept_payment",
			"payment_gateway",
			"payment_button_label",
			"payment_button_help",
			"payments_cb",
			"amount_field",
			"amount_based_on_field",
			"amount",
			"currency",
		)

		for fieldname in fieldnames:
			frappe.db.delete("Custom Field", {"name": "Web Form-" + fieldname})

		frappe.clear_cache(doctype="Web Form")


def before_install():
	# TODO: remove this
	# This is done for erpnext CI patch test
	#
	# Since we follow a flow like install v14 -> restore v10 site
	# -> migrate to v12, v13 and then v14 again
	#
	# This app fails installing when the site is restored to v10 as
	# a lot of apis don;t exist in v10 and this is a (at the moment) required app for erpnext.
	if not frappe.get_meta("Module Def").has_field("custom"):
		return False


@contextmanager
def erpnext_app_import_guard():
	gitlab_link = '<a href="https://gitlab.com/dokos/dokos">Gitlab</a>'
	msg = _("Dokos app is not installed. Please install it from {0}").format(gitlab_link)
	try:
		yield
	except ImportError:
		frappe.throw(msg, title=_("Missing Dokos App"))
