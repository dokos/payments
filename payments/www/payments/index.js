frappe.ready(() => {
	document.getElementById("payment-method-selection-page").addEventListener('click', (e) => {
		const button = e.target.closest(".payment-button");
		if (!button || !button.getAttribute("data-mode-of-payment")) { return; }

		button.disabled = true;
		frappe.freeze({{ _("Redirecting...") | json }})

		const timeout = setTimeout(reenableButton, 10000); // Handle network timeout
		window.addEventListener("pageshow", reenableButton, { once: true }); // Handle browser back button

		frappe.call({
			method: "payments.www.payments.index.get_payment_url",
			args: {
				reference_doctype: button.getAttribute("data-reference_doctype"),
				reference_name: button.getAttribute("data-reference_name"),
				mode_of_payment: button.getAttribute("data-mode-of-payment")
			}
		}).then(r => {
			if (r.message) {
				window.location.href = r.message;
			} else {
				frappe.msgprint({{ _("An error occured. <br>Please contact us.") | json }})
			}
		});

		function reenableButton() {
			button.disabled = false;
			frappe.unfreeze();
			clearTimeout(timeout);
		};
	})
})