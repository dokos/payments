# Copyright (c) 2019, Dokos SAS and Contributors
# License: GNU General Public License v3. See license.txt


import frappe
from frappe import _
from frappe.utils import fmt_money, get_url

ICON_MAP = {
	"Bank Draft": "fas fa-file-invoice",
	"Cash": "fas fa-money-bill-alt",
	"Cheque": "fas fa-money-check-alt",
	"Credit Card": "far fa-credit-card",
	"Wire Transfer": "fas fa-university",
	"Other": "fas fa-coins",
}


def get_context(context):
	context.no_cache = 1
	context.show_sidebar = False

	context.enabled_mode_of_payments = []
	reference_document = None

	if frappe.form_dict.link and frappe.db.exists("Payment Request", dict(payment_key=frappe.form_dict.link)):
		reference_document = frappe.get_doc("Payment Request", dict(payment_key=frappe.form_dict.link))
	elif all(k in frappe.form_dict for k in ("reference_doctype", "reference_name")):
		reference_document = frappe.get_doc(
			frappe.form_dict.reference_doctype, frappe.form_dict.reference_name
		)

	if not reference_document or reference_document.docstatus != 1:
		return

	if reference_document:
		gateway = None
		if reference_document.get("payment_gateway"):
			gateway = reference_document.get("payment_gateway")
		elif reference_document.get("mode_of_payment"):
			gateway = frappe.db.get_value("Mode of Payment", reference_document.get("mode_of_payment"), "payment_gateway")

		context.enabled_mode_of_payments = get_modes_of_payment(company=reference_document.company, gateway=gateway)

	if hasattr(reference_document, "get_payment_status"):
		reference_document.run_method("get_payment_status")

	if len(context.enabled_mode_of_payments) == 1:
		if payment_url := get_payment_url(
			reference_doctype=reference_document.doctype,
			reference_name=reference_document.name,
			mode_of_payment=context.enabled_mode_of_payments[0].name,
		):
			frappe.db.commit()  # Explicitely commit since the request is GET
			frappe.local.flags.redirect_location = payment_url
			raise frappe.Redirect

	context.reference_doctype = reference_document.doctype
	context.reference_name = reference_document.name
	context.subject = reference_document.get("subject") or reference_document.get("description")
	if amount := reference_document.get("grand_total") or reference_document.get("amount"):
		context.formattedAmount = fmt_money(amount, currency=reference_document.get("currency"))
	context.icon_map = ICON_MAP


def get_modes_of_payment(company=None, gateway=None):
	"""
	This hook should return a list of modes of payment with at least two values: `name` and `icon`
	An additional value `portal_description` can be provided for modes of payment not linked to a payment gateway
	"""
	if methods := frappe.get_hooks("authorized_modes_of_payment"):
		enabled_modes_of_payment = frappe.get_attr(methods[-1])(company)
		if gateway:
			enabled_modes_of_payment = [mop for mop in enabled_modes_of_payment if mop.get("payment_gateway") == gateway]

	else:
		fields = ["name", "name as payment_gateway"]
		if frappe.get_meta("Payment Gateway").has_field("icon"):
			fields.append("icon")
		filters = {"disabled": 0}
		if gateway:
			filters["name"] = gateway
		enabled_modes_of_payment = frappe.get_all("Payment Gateway", filters=filters, fields=fields)

	return enabled_modes_of_payment


@frappe.whitelist(allow_guest=True)
def get_payment_url(reference_doctype, reference_name, mode_of_payment):
	reference_document = frappe.get_doc(reference_doctype, reference_name)
	payment_gateway = None
	modes_of_payment = get_modes_of_payment(reference_document.get("company"))
	if payment_gateways := [
		mop.get("payment_gateway") for mop in modes_of_payment if mop.name == mode_of_payment
	]:
		payment_gateway = payment_gateways[0]

	if frappe.get_meta(reference_doctype).has_field("mode_of_payment"):
		reference_document.db_set("mode_of_payment", mode_of_payment)

	if frappe.get_meta(reference_doctype).has_field("payment_gateway"):
		reference_document.db_set("payment_gateway", payment_gateway)

	if frappe.get_meta(reference_doctype).has_field("payment_gateway_account"):
		reference_document.db_set("payment_gateway_account", None)

	if not payment_gateway:
		description = _("Please contact us to get the payment instructions")
		if descriptions := [
			mop.get("portal_description")
			for mop in modes_of_payment
			if mop.get("portal_description") and mop.name == mode_of_payment
		]:
			description = descriptions[0]

		return frappe.redirect_to_message(
			_("Payment Instructions"),
			description,
			context=dict(
				primary_action="/me",
				primary_label=_("My Account"),
				fullpage=True,
			),
		)

	if hasattr(reference_document, "get_payment_url"):
		return reference_document.get_payment_url(payment_gateway)
	else:
		return get_url("/payment-failed")
