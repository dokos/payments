# Copyright (c) 2019, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

import frappe
import frappe.www.list
from erpnext.controllers.website_list_for_contact import get_customers_suppliers
from frappe import _
from frappe.www.me import get_context as get_frappe_context

from payments.payment_gateways.doctype.stancer_settings.api import StancerCardsAPI
from payments.payment_gateways.doctype.stripe_settings.api import StripePaymentMethod
from payments.templates.pages.stripe_checkout import get_api_key


def _get_balance(*args, **kwargs):
	try:
		from bookings.bookings.doctype.booking_credit.booking_credit import get_balance
		return get_balance(*args, **kwargs)
	except Exception:
		return {}


no_cache = 1


def get_context(context):
	if frappe.session.user == "Guest":
		frappe.throw(_("You need to be logged in to access this page"), frappe.PermissionError)

	get_frappe_context(context)

	context.enable_stripe = False
	context.publishable_key = False
	context.stripe_payment_methods = []
	context.stancer_payment_methods = []
	context.enable_gocardless = False
	context.lang = frappe.local.lang
	context.credits_balance = {}

	active_tokens = frappe.get_all(
		"OAuth Bearer Token",
		filters=[["user", "=", frappe.session.user]],
		fields=["client"],
		distinct=True,
		order_by="creation",
	)
	context.third_party_apps = True if active_tokens else False

	customers, suppliers = get_customers_suppliers("Customer", frappe.session.user)
	if customers:
		customer = frappe.get_cached_doc("Customer", customers[0])
		for payment_gateways_reference in customer.payment_gateways_references:
			gateway = frappe.db.get_value(
				"Payment Gateway",
				payment_gateways_reference.payment_gateway,
				["gateway_controller", "gateway_settings"],
				as_dict=True,
			)
			if gateway.gateway_settings == "Stripe Settings":
				context.enable_stripe = True
				context.stripe_payment_methods = get_stripe_customer_payment_methods(
					gateway.gateway_controller, customer.name, payment_gateways_reference.customer_id
				)
				if not context.publishable_key:
					context.publishable_key = get_api_key(gateway.gateway_controller)

			elif gateway.gateway_settings == "Stancer Settings" and not context.stancer_payment_methods:
				settings = frappe.get_cached_doc("Stancer Settings", gateway.gateway_controller)
				if (
					payment_gateways_reference.payment_method == "card"
					and payment_gateways_reference.payment_method_id
				):
					context.stancer_payment_methods = StancerCardsAPI(settings).get_one(
						payment_gateways_reference.payment_method_id
					)

		context.credits_balance = _get_balance(customer.name)


def get_stripe_customer_payment_methods(controller, customer, stripe_customer_id):
	try:
		stripe_settings = frappe.get_doc("Stripe Settings", controller)
		return StripePaymentMethod(stripe_settings).get_list(stripe_customer_id)
	except Exception:
		frappe.log_error(
			_("[Portal] Stripe payment methods retrieval error for {0}").format(customer),
		)


@frappe.whitelist()
def remove_stripe_payment_card(id):
	try:
		customers, suppliers = get_customers_suppliers("Customer", frappe.session.user)
		if customers:
			customer = frappe.get_cached_doc("Customer", customers[0])
			payment_gateways = frappe.get_all(
				"Payment Gateway",
				filters={"gateway_settings": "Stripe Settings", "disabled": 0},
				pluck="name",
			)
			if stripe_references := customer.get(
				"payment_gateways_references", dict(payment_gateway=("in", payment_gateways))
			):
				gateway_controller = frappe.db.get_value(
					"Payment Gateway", stripe_references[0].payment_gateway, "gateway_controller"
				)
				stripe_settings = frappe.get_doc("Stripe Settings", gateway_controller)
				return StripePaymentMethod(stripe_settings).detach(id)

	except Exception:
		frappe.log_error(_("[Portal] Stripe payment source deletion error"))


@frappe.whitelist()
def remove_stancer_payment_card(id):
	try:
		customers, suppliers = get_customers_suppliers("Customer", frappe.session.user)
		if customers:
			customer = frappe.get_cached_doc("Customer", customers[0])
			for payment_gateways_reference in customer.payment_gateways_references:
				gateway = frappe.db.get_value(
					"Payment Gateway",
					payment_gateways_reference.payment_gateway,
					["gateway_controller", "gateway_settings"],
					as_dict=True,
				)
				if (
					gateway.gateway_settings == "Stancer Settings"
					and payment_gateways_reference.payment_method_id == id
				):
					settings = frappe.get_cached_doc("Stancer Settings", gateway.gateway_controller)
					result = StancerCardsAPI(settings).delete(id)
					payment_gateways_reference.payment_method = None
					payment_gateways_reference.payment_method_id = None
					customer.flags.ignore_permissions = True
					customer.save()
					return result
	except Exception:
		frappe.log_error(_("[Portal] Stancer payment method deletion error"))


@frappe.whitelist()
def add_new_payment_card(payment_method):
	try:
		customers, suppliers = get_customers_suppliers("Customer", frappe.session.user)
		if customers:
			customer = frappe.get_cached_doc("Customer", customers[0])
			payment_gateways = frappe.get_all(
				"Payment Gateway",
				filters={"gateway_settings": "Stripe Settings", "disabled": 0},
				pluck="name",
			)
			if stripe_references := customer.get(
				"payment_gateways_references", dict(payment_gateway=("in", payment_gateways))
			):
				gateway_controller = frappe.db.get_value(
					"Payment Gateway", stripe_references[0].payment_gateway, "gateway_controller"
				)
				stripe_settings = frappe.get_doc("Stripe Settings", gateway_controller)
				stripe_payment_method = StripePaymentMethod(stripe_settings).attach(
					payment_method, stripe_references[0].customer_id
				)
				if stripe_payment_method:
					stripe_settings.register_customer(stripe_references[0].customer_id, stripe_payment_method.get("type"), stripe_payment_method.get("id"), customer.name)

				return stripe_payment_method

	except Exception:
		frappe.log_error(_("[Portal] New stripe payment source registration error"))
